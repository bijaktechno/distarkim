<?php

namespace App\Http\Controllers;

use App\Makam;
use App\Jenazah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class JenazahController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $makam = Makam::all();

        return view('admin.jenazah.index', compact('makam'));
    }

    public function get()
    {

        $jenazah = Jenazah::all();

        return datatables()->of($jenazah)
            ->editColumn('created_at', '{{ date("d F Y", strtotime($created_at)) }}')
            ->editColumn('updated_at', '{{ date("d F Y", strtotime($updated_at)) }}')
            ->editColumn('jenazah_meninggal_date', '{{ date("d F Y", strtotime($jenazah_meninggal_date)) }}')
            ->addColumn('makam_name', function ($jenazah) {
                return $jenazah->makam->makam_name;})
            ->addColumn('publication_status', function ($jenazah) {
                if ($jenazah->publication_status == 1) {
                    return '<a href="' . route('admin.unpublishedJenazahRoute', $jenazah->id) . '" class="btn btn-success btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Unpublished"><i class="icon fa fa-arrow-down"></i>Published</a>';
                }
                return '<a href="' . route('admin.publishedJenazahRoute', $jenazah->id) . '" class="btn btn-warning btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Published"><i class="icon fa fa-arrow-up"></i> Unpublished</a>';})
            ->addColumn('action', function ($jenazah) {
                return '<button class="btn btn-info btn-xs view-button" data-id="' . $jenazah->id . '" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button> <button class="btn btn-primary btn-xs edit-button" data-id="' . $jenazah->id . '" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button> <button class="btn btn-danger btn-xs delete-button" data-id="' . $jenazah->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})
            ->rawColumns(['makam_name', 'action', 'publication_status'])
            ->setRowId('id')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $validator = Validator::make($request->all(), [
            'makam_id' => 'required',
            'jenazah_name' => 'required|max:150',
            'jenazah_nik' => 'required',
            'jenazah_ahli_waris' => 'required',
            'jenazah_meninggal_date' => 'required',
            'jenazah_kubur_date' => 'required',
            'jenazah_agama' => 'required',
            'jenazah_status' => 'required',
            'publication_status' => 'required',
        ], [
            'makam_id.required' => 'Nama Makam is required.',
        ]);

        if ($validator->passes()) {
            $jenazah = Jenazah::create([
                'makam_id' => $request->input('makam_id'),
                'jenazah_name' => $request->input('jenazah_name'),
                'jenazah_nik' => $request->input('jenazah_nik'),
                'jenazah_ahli_waris' => $request->input('jenazah_ahli_waris'),
                'jenazah_meninggal_date' => $request->input('jenazah_meninggal_date'),
                'jenazah_kubur_date' => $request->input('jenazah_kubur_date'),
                'jenazah_agama' => $request->input('jenazah_agama'),
                'jenazah_status' => $request->input('jenazah_status'),
                'publication_status' => $request->input('publication_status'),
            ]);
            $jenazah_id = $jenazah->id;

            if (!empty($jenazah_id)) {
                $request->session()->flash('message', 'Jenazah add successfully.');
            } else {
                $request->session()->flash('exception', 'Operation failed !');
            }

            return Response::json(['success' => '1']);
        }
        return Response::json(['errors' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jenazah = Jenazah::where('id', $id)->with('makam')
            ->first();
        return json_encode($jenazah);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jenazah = Jenazah::find($id);

        $validator = $validator = Validator::make($request->all(), [

            'makam_id' => 'required',
            'jenazah_name' => 'required|max:150',
            'jenazah_nik' => 'required',
            'jenazah_ahli_waris' => 'required',
            'jenazah_meninggal_date' => 'required',
            'jenazah_kubur_date' => 'required',
            'jenazah_agama' => 'required',
            'jenazah_status' => 'required',
            'publication_status' => 'required',
        ], [
            'makam_id.required' => 'Nama Makam is required.',
        ]);

        if ($validator->passes()) {
            $jenazah->makam_id = $request->get('makam_id');
            $jenazah->jenazah_name = $request->get('jenazah_name');
            $jenazah->jenazah_nik = $request->get('jenazah_nik');
            $jenazah->jenazah_ahli_waris = $request->get('jenazah_ahli_waris');
            $jenazah->jenazah_meninggal_date = $request->get('jenazah_meninggal_date');
            $jenazah->jenazah_kubur_date = $request->get('jenazah_kubur_date');
            $jenazah->jenazah_agama = $request->get('jenazah_agama');
            $jenazah->jenazah_status = $request->get('jenazah_status');
            $jenazah->publication_status = $request->get('publication_status');
            $affected_row = $jenazah->save();

            if (!empty($affected_row)) {
                $request->session()->flash('message', 'Jenazah update successfully.');
            } else {
                $request->session()->flash('exception', 'Operation failed !');
            }
            return Response::json(['success' => '1']);
        }
        return Response::json(['errors' => $validator->errors()]);
    }


    public function published($id) {
        $affected_row = Jenazah::where('id', $id)
            ->update(['publication_status' => 1]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Published successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }

    public function unpublished($id) {
        $affected_row = Jenazah::where('id', $id)
            ->update(['publication_status' => 0]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Unpublished successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenazah = Jenazah::find($id);
        if (count($jenazah)) {
            //$jenazah->posts()->detach();
            $jenazah->delete();
            return redirect()->back()->with('message', 'Jenazah delete successfully.');
        } else {
            return redirect()->back()->with('exception', 'Jenazah not found !');
        }
    }
}
