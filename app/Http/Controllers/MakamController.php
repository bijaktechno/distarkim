<?php

namespace App\Http\Controllers;

use App\Makam;
use App\Kelurahan;
use App\Kecamatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class MakamController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelurahan = Kelurahan::all();
        // $kecamatan = Kecamatan::all();
        // return Response::json($kelurahan);
        // dd($kelurahan);
        return view('admin.makam.index', compact('kelurahan'));
    }

    public function get()
    {

        $makam = Makam::all();

        return datatables()->of($makam)
            ->editColumn('created_at', '{{ date("d F Y", strtotime($created_at)) }}')
            ->editColumn('updated_at', '{{ date("d F Y", strtotime($updated_at)) }}')
            ->addColumn('kelurahan_name', function ($makam) {
                return $makam->kelurahan->kelurahan_name;})
            ->addColumn('publication_status', function ($makam) {
                if ($makam->publication_status == 1) {
                    return '<a href="' . route('admin.unpublishedMakamRoute', $makam->id) . '" class="btn btn-success btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Unpublished"><i class="icon fa fa-arrow-down"></i>Published</a>';
                }
                return '<a href="' . route('admin.publishedMakamRoute', $makam->id) . '" class="btn btn-warning btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Published"><i class="icon fa fa-arrow-up"></i> Unpublished</a>';})
            ->addColumn('action', function ($makam) {
                return '<button class="btn btn-info btn-xs view-button" data-id="' . $makam->id . '" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button> <button class="btn btn-primary btn-xs edit-button" data-id="' . $makam->id . '" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button> <button class="btn btn-danger btn-xs delete-button" data-id="' . $makam->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})
            ->rawColumns(['kelurahan_name', 'action', 'publication_status'])
            ->setRowId('id')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $validator = Validator::make($request->all(), [
            'kelurahan_id' => 'required',
            'makam_name' => 'required|min:5|max:150',
            'makam_address' => 'required|max:400',
            'publication_status' => 'required',
        ], [
            'kelurahan_id.required' => 'Nama kelurahan is required.',
        ]);

        if ($validator->passes()) {
            $makam = Makam::create([
                'kelurahan_id' => $request->input('kelurahan_id'),
                'makam_name' => $request->input('makam_name'),
                'makam_address' => $request->input('makam_address'),
                'publication_status' => $request->input('publication_status'),
            ]);
            $makam_id = $makam->id;

            if (!empty($makam_id)) {
                $request->session()->flash('message', 'Makam add successfully.');
            } else {
                $request->session()->flash('exception', 'Operation failed !');
            }

            return Response::json(['success' => '1']);
        }
        return Response::json(['errors' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $makam = Makam::where('id', $id)->with('kelurahan')
            ->first();
        return json_encode($makam);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $makam = Makam::find($id);

        $validator = $validator = Validator::make($request->all(), [

            'kelurahan_id' => 'required',
            'makam_name' => 'required|min:5|max:150',
            'makam_address' => 'required|max:400',
            'publication_status' => 'required',
        ], [
            'kelurahan_id.required' => 'Nama kelurahan is required.',
        ]);

        if ($validator->passes()) {
            $makam->kelurahan_id = $request->get('kelurahan_id');
            $makam->makam_name = $request->get('makam_name');
            $makam->makam_address = $request->get('makam_address');
            $makam->publication_status = $request->get('publication_status');
            $affected_row = $makam->save();

            if (!empty($affected_row)) {
                $request->session()->flash('message', 'Makam update successfully.');
            } else {
                $request->session()->flash('exception', 'Operation failed !');
            }
            return Response::json(['success' => '1']);
        }
        return Response::json(['errors' => $validator->errors()]);
    }


    public function published($id) {
        $affected_row = Makam::where('id', $id)
            ->update(['publication_status' => 1]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Published successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }

    public function unpublished($id) {
        $affected_row = Makam::where('id', $id)
            ->update(['publication_status' => 0]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Unpublished successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $makam = Makam::find($id);
        if (count($makam)) {
            //$makam->posts()->detach();
            $makam->delete();
            return redirect()->back()->with('message', 'Makam delete successfully.');
        } else {
            return redirect()->back()->with('exception', 'Makam not found !');
        }
    }
}
