<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenazah extends Model
{
	protected $table = 'jenazah';

    protected $fillable = [
        'makam_id', 'jenazah_name', 'jenazah_nik', 'jenazah_ahli_waris', 'jenazah_meninggal_date', 'jenazah_kubur_date', 'jenazah_agama', 'jenazah_status', 'publication_status'
    ];

    public function makam() {
        return $this->belongsTo(Makam::class);
    }
}
