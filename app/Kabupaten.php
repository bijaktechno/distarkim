<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
	protected $table = 'kabupaten';

    protected $fillable = [
        'propinsi_id', 'kabupaten_name'
    ];

	public function kecamatan() {
		return $this->hasMany(Kecamatan::class);
	}
}
