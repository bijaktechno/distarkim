<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
	protected $table = 'kecamatan';

    protected $fillable = [
        'kabupaten_id', 'kecamatan_name'
    ];

	public function kelurahan() {
		return $this->hasMany(Kelurahan::class);
	}

    public function kabupaten() {
        return $this->belongsTo(Kabupaten::class);
    }
}
