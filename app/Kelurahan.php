<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
	protected $table = 'kelurahan';

    protected $fillable = [
        'kecamatan_id', 'kelurahan_name'
    ];

	public function makam() {
		return $this->hasMany(Makam::class);
	}

    public function kecamatan() {
        return $this->belongsTo(Kecamatan::class);
    }
}
