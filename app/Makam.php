<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Makam extends Model
{
	protected $table = 'makam';

    protected $fillable = [
        'kelurahan_id', 'makam_name', 'makam_address', 'publication_status'
    ];

	public function jenazah() {
		return $this->hasMany(Jenazah::class);
	}

    public function kelurahan() {
        return $this->belongsTo(Kelurahan::class);
    }
}
