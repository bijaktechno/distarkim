<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageFile extends Model
{
	protected $fillable = [
		'page_id', 'file_title', 'file_path',
	];


	public function page() {
		return $this->belongsTo(Page::class);
	}
}
