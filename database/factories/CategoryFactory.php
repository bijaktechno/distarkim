<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {

	$word = ucfirst($faker->word);
	$category_slug = $faker->word . '_' . $faker->word;
	$meta_keywords = $faker->word . ', ' . $faker->word . ', ' . $faker->word;

	return [
		[
			'id' => 1,
			'user_id' => 1,
			'category_name' => 'Berita',
			'category_slug' => 'berita',
			'publication_status' => 1,
			'meta_title' => 'Berita',
			'meta_keywords' => 'Berita',
			'meta_description' => 'Berita',
		],
		[
			'id' => 2,
			'user_id' => 1,
			'category_name' => 'Pengumuman',
			'category_slug' => 'pengumuman',
			'publication_status' => 1,
			'meta_title' => 'Pengumuman',
			'meta_keywords' => 'Pengumuman',
			'meta_description' => 'Pengumuman',
		]

	];
});
