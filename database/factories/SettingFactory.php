<?php

use Faker\Generator as Faker;

$factory->define(App\Setting::class, function (Faker $faker) {
	return [
		'website_title' => 'DISTARKIM',
		'logo' => 'logo.png',
		'favicon' => 'favicon.png',
		'about_us' => 'Visi Dinas Tata Ruang dan Permukiman Kabupaten Purwakarta merupakan penjabaran dari Visi Pemerintah Kabupaten Purwakarta dan Misi ke-2(dua) Kabupaten Purwakarta sebagaimana telah tertuang dalam RPJMD Kabupaten Purwakarta Tahun 2013-2018.',
		'copyright' => 'Copyright 2018 <a href="http://distarkim.purwakartakab.go.id/" target="_blank">Distarkim</a>, All rights reserved.',
		'email' => 'distarkim@gmail.com',
		'phone' => '+8801717888464',
		'mobile' => '+8801761913331',
		'fax' => '808080',
		'address_line_one' => 'House# 83, Road# 16, Sector# 11',
		'address_line_two' => 'Purwakarta',
		'state' => 'Purwakarta',
		'city' => 'Purwakarta',
		'zip' => '12345',
		'country' => 'Indonesia',
		'map_iframe' => '
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1981.8586994225045!2d107.44609445791026!3d-6.55731479881435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e690e4ef9d22ce7%3A0x6b12d62f529446a7!2sDinas+Tata+Ruang+dan+Permukiman+Kabupaten+Purwakarta!5e0!3m2!1sid!2sid!4v1541513363710" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
		'facebook' => 'https://facebook.com/distarkimpurwakarta',
		'twitter' => 'https://twitter.com/distarkimpwk',
		'google_plus' => '',
		'linkedin' => '',
		'meta_title' => 'DISTARKIM',
		'meta_keywords' => 'DISTARKIM',
		'meta_description' => 'DISTARKIM.',
		'gallery_meta_title' => 'DISTARKIM',
		'gallery_meta_keywords' => 'DISTARKIM',
		'gallery_meta_description' => 'DISTARKIM.',
	];
});
