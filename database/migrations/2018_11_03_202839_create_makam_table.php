<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('makam', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kelurahan_id')->unsigned()->index();
            $table->string('makam_name');
            $table->text('makam_address')->nullable();
            $table->tinyInteger('publication_status')->default(0);
            $table->foreign('kelurahan_id')->references('id')->on('kelurahan')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('makam');
    }
}
