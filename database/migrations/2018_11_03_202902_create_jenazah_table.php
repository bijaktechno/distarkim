<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenazahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenazah', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('makam_id')->unsigned()->index();
            $table->string('jenazah_name');
            $table->string('jenazah_nik');
            $table->string('jenazah_ahli_waris');
            $table->date('jenazah_meninggal_date');
            $table->date('jenazah_kubur_date');
            $table->string('jenazah_agama');
            $table->string('jenazah_status');
            $table->tinyInteger('publication_status')->default(0);
            $table->foreign('makam_id')->references('id')->on('makam')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenazah');
    }
}
