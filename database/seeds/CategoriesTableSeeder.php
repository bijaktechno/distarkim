<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\Category::class, 15)->create();

    	DB::table('categories')->insert([
            [
				'id' => 1,
				'user_id' => 1,
				'category_name' => 'Berita',
				'category_slug' => 'berita',
				'publication_status' => 1,
				'meta_title' => 'Berita',
				'meta_keywords' => 'Berita',
				'meta_description' => 'Berita',
			],
			[
				'id' => 2,
				'user_id' => 1,
				'category_name' => 'Pengumuman',
				'category_slug' => 'pengumuman',
				'publication_status' => 1,
				'meta_title' => 'Pengumuman',
				'meta_keywords' => 'Pengumuman',
				'meta_description' => 'Pengumuman',
			]
        ]);
    }
}
