<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$this->call(UsersTableSeeder::class);
		$this->call(CategoriesTableSeeder::class);
		$this->call(SettingsTableSeeder::class);
		$this->call(PagesTableSeeder::class);
		$this->call(PageFilesTableSeeder::class);
		$this->call(KabupatenSeeder::class);
		$this->call(KecamatanSeeder::class);
		$this->call(KelurahanSeeder::class);
		$this->call(MakamSeeder::class);
	}
}
