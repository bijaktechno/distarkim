<?php

use Illuminate\Database\Seeder;

class KabupatenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	DB::table('kabupaten')->insert([

            'id' => 3214,
			'propinsi_id' => 32,
            'kabupaten_name' => 'PURWAKARTA',
        ]);
    }
}
