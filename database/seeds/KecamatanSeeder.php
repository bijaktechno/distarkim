<?php

use Illuminate\Database\Seeder;

class KecamatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('kecamatan')->insert([
			['id' => 3214010, 'kecamatan_name' => 'JATILUHUR', 'kabupaten_id' => 3214],
			['id' => 3214011, 'kecamatan_name' => 'SUKASARI', 'kabupaten_id' => 3214],
			['id' => 3214020, 'kecamatan_name' => 'MANIIS', 'kabupaten_id' => 3214],
			['id' => 3214030, 'kecamatan_name' => 'TEGAL WARU', 'kabupaten_id' => 3214],
			['id' => 3214040, 'kecamatan_name' => 'PLERED', 'kabupaten_id' => 3214],
			['id' => 3214050, 'kecamatan_name' => 'SUKATANI', 'kabupaten_id' => 3214],
			['id' => 3214060, 'kecamatan_name' => 'DARANGDAN', 'kabupaten_id' => 3214],
			['id' => 3214070, 'kecamatan_name' => 'BOJONG', 'kabupaten_id' => 3214],
			['id' => 3214080, 'kecamatan_name' => 'WANAYASA', 'kabupaten_id' => 3214],
			['id' => 3214081, 'kecamatan_name' => 'KIARAPEDES', 'kabupaten_id' => 3214],
			['id' => 3214090, 'kecamatan_name' => 'PASAWAHAN', 'kabupaten_id' => 3214],
			['id' => 3214091, 'kecamatan_name' => 'PONDOK SALAM', 'kabupaten_id' => 3214],
			['id' => 3214100, 'kecamatan_name' => 'PURWAKARTA', 'kabupaten_id' => 3214],
			['id' => 3214101, 'kecamatan_name' => 'BABAKANCIKAO', 'kabupaten_id' => 3214],
			['id' => 3214110, 'kecamatan_name' => 'CAMPAKA', 'kabupaten_id' => 3214],
			['id' => 3214111, 'kecamatan_name' => 'CIBATU', 'kabupaten_id' => 3214],
			['id' => 3214112, 'kecamatan_name' => 'BUNGURSARI', 'kabupaten_id' => 3214]

        ]);
    }
}
