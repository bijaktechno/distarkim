<?php

use Illuminate\Database\Seeder;

class PageFilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('page_files')->insert([
            [
				'page_id' => 5,
				'file_title' => 'data-taman.pdf',
				'file_path' => '5.data-taman.pdf',
			],
			[
				'page_id' => 5,
				'file_title' => '1Renstra-2017-2018.pdf',
				'file_path' => '5.1Renstra-2017-2018.pdf',
			],
			[
				'page_id' => 5,
				'file_title' => '1Renja-2018.pdf',
				'file_path' => '5.1Renja-2018.pdf',
			],
			[
				'page_id' => 5,
				'file_title' => '1IKU-Distarkim-2017-2018.pdf',
				'file_path' => '5.1IKU-Distarkim-2017-2018.pdf',
			],
			[
				'page_id' => 6,
				'file_title' => 'Informasi IMB.pdf',
				'file_path' => '6.Informasi IMB.pdf',
			],
			[
				'page_id' => 4,
				'file_title' => 'TUPOKSI.pdf',
				'file_path' => '4.TUPOKSI.pdf',
			]
        ]);
    }
}
