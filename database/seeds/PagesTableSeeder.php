<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

    	DB::table('pages')->insert([
            [
				'id' => 1,
				'user_id' => 1,
				'page_name' => 'Struktur Organisasi',
				'page_slug' => 'struktur_organisasi',
				'page_content' => '<p>Dalam menjalankan visi dan misi dinas, Kepala Dinas dibantu oleh 1 Sekretariat dan 4 bidang, dan 1 Kepala UPTD, dimana masing-masing bidang membawahi 3 seksi/sub bagian. Struktur Organisasi Dinas Tata Ruang dan Permukiman Kabupaten Purwakarta dapat dilihat dalam gambar di atas</p>',
				'page_featured_image' => '1.jpg',
				'page_type' => 0,
				'meta_title' => 'Struktur Organisasi',
				'meta_keywords' => 'Struktur Organisasi',
				'meta_description' => 'Struktur Organisasi',
				'publication_status' => 1,
			],
			[
				'id' => 2,
				'user_id' => 1,
				'page_name' => 'Tugas dan Fungsi',
				'page_slug' => 'tugas_dan_fungsi',
				'page_content' => '<p class="fs14 col686868" style="margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;">Tugas dan fungsi Dinas Tata Ruang diatur dalam&nbsp;<br>Peraturan Bupati No.148 Tahun 2016 tentang Kedudukan, Susunan Organisasi, Tugas dan Fungsi Perangkat&nbsp;<br>Daerah sebagai berikut :</p><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><h6 style="margin-top: 0px; margin-bottom: 0.5rem; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; line-height: 1.2; color: rgb(33, 37, 41); font-size: 1rem;">Tugas Pokok :</h6><p class="fs14 col686868" style="margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;">Melaksanakan sebagian urusan pemerintahan daerah di bidang pekerjaan umum lingkup keciptakaryaan, penataan ruang, perumahan dan permukiman berdasarkan asas otonomi dan tugas pembantuan.</p><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><h6 style="margin-top: 0px; margin-bottom: 0.5rem; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; line-height: 1.2; color: rgb(33, 37, 41); font-size: 1rem;">Fungsi :</h6><p style="margin-bottom: 1rem; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><ul class="fs14 col686868" style="margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;"><li>Perumusan kebijakan teknis bidang keciptakaryaan, penataan ruang, dan perumahan;</li><li>Penyelenggaraan sebagian urusan pemerintahan dan pelayanan umum di bidang pekerjaan umum lingkup keciptakaryaan, penataan ruang dan perumahan;</li><li>Pembinaan dan pelaksanaan tugas yang meliputi bidang tata bangunan, pembinaan, pengawasan dan pengendalian, perumahan, permukiman, dan tata ruang.</li><li>Pelaksanaan pelayanan teknis administratif dinas</li><li>Pelaksanaan tugas lain yang diberikan oleh bupati sesuai dengan tugas dan fungsinya.</li></ul>',
				'page_featured_image' => '',
				'page_type' => 0,
				'meta_title' => 'Tugas dan Fungsi',
				'meta_keywords' => 'Tugas dan Fungsi',
				'meta_description' => 'Tugas dan Fungsi',
				'publication_status' => 1,
			],
			[
				'id' => 3,
				'user_id' => 1,
				'page_name' => 'Visi dan Misi',
				'page_slug' => 'visi_misi',
				'page_content' => '<h5 class="col844D28" style="margin-top: 0px; margin-bottom: 0.5rem; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; line-height: 1.2; color: rgb(132, 77, 40); font-size: 1.25rem;">Visi</h5><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><p class="col009141" style="margin-bottom: 1rem; color: rgb(0, 145, 65); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"><i>"Terwujudnya Tata Ruang dan Bangunan serta Sarana dan Prasarana Perumahan dan Permukiman yang Berkarakter dan Berwawasan Lingkungan".</i></p><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><p class="fs14 col686868" style="margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;">Visi Dinas Tata Ruang dan Permukiman Kabupaten Purwakarta merupakan penjabaran dari Visi Pemerintah&nbsp;<br>Kabupaten Purwakarta dan Misi ke-2(dua) Kabupaten Purwakarta sebagaimana telah tertuang dalam RPJMD&nbsp;<br>Kabupaten Purwakarta Tahun 2013-2018</p><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><p class="fs14 col686868" style="margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;">Visi Pembangunan Jangka Menengah Kabupaten Purwakarta Tahun 2013-2018 adalah&nbsp;<br><span style="font-weight: bolder;">"PURWAKARTA BERKARAKTER"</span></p><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><h5 class="col844D28" style="margin-top: 0px; margin-bottom: 0.5rem; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; line-height: 1.2; color: rgb(132, 77, 40); font-size: 1.25rem;">Misi</h5><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><p class="fs14 col686868" style="margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;">Dinas Tata Ruang dan Permukiman Kabupaten Purwakarta menetapkan Misi sebagai berikut :</p><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><p class="fs14 col686868" style="margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;">1. Mewujudkan Sumber Daya Aparatur Yang Profesional Dalam Rangka Meningkatkan Pelayanan Kepada Masyarakat.</p><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><p class="fs14 col686868" style="margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;">2. Meningkatkan Sarana Dan Prasarana Perumahan Dan Permukiman Dalam Rangka Terpenuhinya Kebutuhan Dasar Masyarakat Dan Lingkungan Yang Bersih, Nyaman Dan Aman.</p><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><p class="fs14 col686868" style="margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;">3. Mewujudkan Pelayanan Infrastruktur Wilayah Di Bidang Penyediaan Prasarana dan Sarana Air Bersih.</p><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><p class="fs14 col686868" style="margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;">4. Mewujudkan Penyelenggaraan Penataan Ruang Secara Transparan, Efektif Dan Partisipatif Melalui Kegiatan Pengaturan, Pembinaan, Pelaksanaan Dan Pengawasan Agar Terwujud Penataan Ruang Yang Aman, Nyaman, Produktif Dan Berkelanjutan.</p><p><br style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;"></p><p class="fs14 col686868" style="margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;">5. Mewujudkan Tata Kelola Pertanahan Untuk Kepentingan Pemerintah Daerah Dan Pembangunan Fasilitas Pubik Agar Terwujud Tertib Administrasi Pertanahan.</p><div><br></div>',
				'page_featured_image' => '',
				'page_type' => 0,
				'meta_title' => 'Visi dan Misi',
				'meta_keywords' => 'Visi dan Misi',
				'meta_description' => 'Visi dan Misi',
				'publication_status' => 1,
			],
			[
				'id' => 4,
				'user_id' => 1,
				'page_name' => 'Tupoksi',
				'page_slug' => 'tupoksi',
				'page_content' => '<p>lorem ipsum</p>',
				'page_featured_image' => '',
				'page_type' => 1,
				'meta_title' => 'Tupoksi',
				'meta_keywords' => 'Tupoksi',
				'meta_description' => 'Tupoksi',
				'publication_status' => 1,
			],
			[
				'id' => 5,
				'user_id' => 1,
				'page_name' => 'Data Distarkim',
				'page_slug' => 'data_distarkim',
				'page_content' => '<p>lorem ipsum</p>',
				'page_featured_image' => '',
				'page_type' => 1,
				'meta_title' => 'Data Distarkim',
				'meta_keywords' => 'Data Distarkim',
				'meta_description' => 'Data Distarkim',
				'publication_status' => 1,
			],
			[
				'id' => 6,
				'user_id' => 1,
				'page_name' => 'IMB',
				'page_slug' => 'imb',
				'page_content' => '<p>lorem ipsum</p>',
				'page_featured_image' => '',
				'page_type' => 1,
				'meta_title' => 'IMB',
				'meta_keywords' => 'IMB',
				'meta_description' => 'IMB',
				'publication_status' => 1,
			],
			[
				'id' => 9,
				'user_id' => 1,
				'page_name' => 'Peraturan Perundang-Undangan',
				'page_slug' => 'peraturan_perundang_undangan',
				'page_content' => '<p>lorem ipsum</p>',
				'page_featured_image' => '',
				'page_type' => 1,
				'meta_title' => 'Peraturan Perundang-Undangan',
				'meta_keywords' => 'Peraturan Perundang-Undangan',
				'meta_description' => 'Peraturan Perundang-Undangan',
				'publication_status' => 1,
			],
			[
				'id' => 10,
				'user_id' => 1,
				'page_name' => 'Layanan',
				'page_slug' => 'layanan',
				'page_content' => '<p><b style="color: inherit; font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 30px;">Pelayanan Publik yang dikelola oleh Dinas Tata Ruang dan Permukiman Kabupaten Purwakarta adalah :</b><br></p><p>Di Bidang Tata Ruang, Pertanahan&nbsp; dan Pengawasan Bangunan (TAPERWAS):</p><ol><li>Pembuatan Rekomendasi Ijin Mendirikan Bangunan (IMB)</li><li>Pengesahan Rencana Anggaran Biaya (RAB)</li><li>Pengesahan Gambar Konstruksi Bangunan.</li><li>Pemprosesan dan Pengesahan Site Plan.</li><li>Penerbitan Keterangan Rencana Kabupaten (KRK)</li><li>Pengawasan Bangunan Gedung dan Sarana Prasarana Penunjang&nbsp;</li></ol><p>Dibidang Tata Bangunan:</p><ol><li>Perencanaan Teknis Bangunan Gedung dan Sarana Prasarana Penunjang</li></ol><p>Di Bidang Pertamanan dan Penerangan Jalan Umum</p><ol><li>Registrasi Pemakaman</li></ol><div><br></div>',
				'page_featured_image' => '',
				'page_type' => 1,
				'meta_title' => 'Layanan',
				'meta_keywords' => 'Layanan',
				'meta_description' => 'Layanan',
				'publication_status' => 1,
			]
        ]);
		// factory(App\Page::class, 5)->create();
	}
}
