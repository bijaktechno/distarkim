<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\User::class, 10)->create();

        
    	DB::table('users')->insert([

            'id' => 1,
			'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('secret'),
			'role' => 'admin',
			'remember_token' => str_random(10),
        ]);
    }
}
