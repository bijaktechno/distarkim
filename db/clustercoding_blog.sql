-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2018 at 08:05 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clustercoding_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `user_id`, `category_name`, `category_slug`, `meta_title`, `meta_keywords`, `meta_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ratione', 'quod_facere', 'Ratione', 'recusandae, fuga, illo', 'Veritatis officiis iusto consequuntur necessitatibus et ut. Officia fuga quis doloribus tenetur. Quos quibusdam recusandae esse dolorum et numquam.', 1, '2018-03-06 11:10:01', '2018-04-12 21:36:24'),
(2, 1, 'Voluptatem', 'asperiores_aut', 'Voluptatem', 'porro, molestiae, fuga', 'Voluptas et autem alias. Expedita dicta soluta porro. Assumenda fugiat quaerat maiores laboriosam illo et fugiat.', 1, '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(3, 1, 'Dolores', 'explicabo_repellat', 'Dolores', 'voluptas, molestias, omnis', 'Voluptatem exercitationem qui mollitia error. Inventore sunt non dicta sit quod a. Velit commodi autem saepe.', 0, '2018-03-06 11:10:01', '2018-04-09 08:13:00'),
(4, 1, 'Consequuntur', 'quo_consequatur', 'Consequuntur', 'et, omnis, hic', 'Dicta numquam dolorum iste eum unde. Voluptas voluptatem dolorem voluptatibus unde. Rerum ad maiores non et.', 0, '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(5, 1, 'Magni', 'sed_eligendi', 'Magni', 'qui, minima, corrupti', 'Velit et laboriosam delectus molestias amet porro. Fuga voluptate cum neque amet. Quos molestiae animi eveniet accusantium non.', 0, '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(6, 1, 'Expedita', 'exercitationem_molestiae', 'Expedita', 'in, omnis, delectus', 'Suscipit aut facilis saepe voluptatibus quos et animi. Consequatur illo asperiores exercitationem ullam id esse officiis.', 1, '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(7, 1, 'Voluptatem', 'eveniet_vitae', 'Voluptatem', 'voluptatibus, voluptatem, recusandae', 'Ipsum qui iste necessitatibus vel corporis. Odit sequi aut dolor aut nisi. Soluta illum perspiciatis aut atque magni in nemo.', 1, '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(8, 1, 'Sapiente', 'temporibus_laudantium', 'Sapiente', 'delectus, velit, quia', 'Odio expedita consequuntur sint. Officiis enim fuga dolor quas rerum. Ut hic quo minus explicabo consequatur voluptate.', 1, '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(9, 1, 'Corporis', 'aliquam_veritatis', 'Corporis', 'dolorem, omnis, non', 'Esse et quibusdam qui voluptatem. Accusamus dolores voluptatem rerum vel id aperiam.', 1, '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(10, 1, 'In', 'provident_asperiores', 'In', 'eos, fuga, aliquam', 'Molestiae sed sint molestias magni ad cum ducimus aut. Quo non eos ratione. Aperiam sunt quia eveniet et vitae voluptate neque.', 1, '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(11, 1, 'Ea', 'sint_accusamus', 'Ea', 'autem, commodi, eveniet', 'Cum et voluptas illo ea veniam tempore. Architecto accusantium maiores sint consequatur est animi aut. Voluptatem ex quod quod.', 1, '2018-03-06 11:10:02', '2018-03-06 11:10:02'),
(12, 1, 'Impedit', 'id_enim', 'Impedit', 'sunt, velit, nostrum', 'Sint voluptas molestiae dolorem quia. Adipisci aperiam ducimus neque vel. Maiores velit consequatur voluptate repudiandae enim.', 0, '2018-03-06 11:10:02', '2018-03-06 11:10:02'),
(13, 1, 'Consequatur', 'et_aut', 'Consequatur', 'aliquam, dignissimos, quo', 'Non rerum sed consequuntur vitae. Blanditiis debitis similique eum itaque. Quos suscipit ut odio quisquam sed rerum quibusdam voluptatum.', 1, '2018-03-06 11:10:02', '2018-03-06 11:10:02'),
(14, 1, 'Et', 'eum_quod', 'Et', 'doloremque, vel, eum', 'Harum rerum et laboriosam occaecati. Inventore impedit perferendis occaecati voluptatem facere. Officiis iste qui similique numquam saepe.', 0, '2018-03-06 11:10:02', '2018-03-06 11:10:02'),
(15, 1, 'Voluptate', 'voluptatibus_eos', 'Voluptate', 'quos, tempore, ipsam', 'Facere rerum odit ut sit. Non veniam laborum odit dolorum aut eligendi. Autem numquam est quis sit fugiat perspiciatis soluta pariatur.', 0, '2018-03-06 11:10:02', '2018-03-08 13:06:20');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `parent_comment_id` int(10) UNSIGNED DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `parent_comment_id`, `comment`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 14, 5, NULL, 'Non natus aspernatur dolorem.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(2, 13, 8, NULL, 'Neque aperiam aut ea dolor eos sed.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(3, 19, 1, NULL, 'Et aut illum libero vel quaerat.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(4, 5, 15, NULL, 'Et quia et exercitationem officia.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(5, 10, 2, NULL, 'Aliquam sed ea rem qui rerum ut.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(6, 18, 3, NULL, 'Est perferendis recusandae doloremque voluptatem ad assumenda omnis.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(7, 18, 18, NULL, 'Nisi repellat eius sunt consequatur voluptatem et.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(8, 4, 1, NULL, 'Impedit qui sunt quod tempora in voluptate iure possimus.', 1, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(9, 5, 12, NULL, 'Facere neque alias corrupti voluptatem excepturi maiores quia et.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(10, 20, 11, NULL, 'Explicabo at totam ullam voluptatibus quis unde.', 1, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(11, 20, 10, NULL, 'Sed voluptas sequi maiores.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(12, 1, 4, NULL, 'Consequatur a esse nihil illum.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(14, 2, 19, NULL, 'Corporis quasi ex iusto placeat.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(15, 13, 16, NULL, 'Quam ad libero architecto facere sit corrupti placeat.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(16, 12, 13, NULL, 'Necessitatibus debitis aut facilis aut nam temporibus.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(17, 19, 19, NULL, 'Dolore et ullam consequatur ut.', 1, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(18, 13, 8, NULL, 'Nemo qui quisquam ea.', 1, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(19, 2, 19, NULL, 'Perferendis corporis ullam culpa itaque natus est sequi qui.', 1, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(20, 13, 8, NULL, 'Quod quos maiores accusamus quidem sed culpa culpa.', 0, '2018-03-14 11:19:54', '2018-03-14 11:19:54'),
(21, 1, 8, 2, 'Test comment', 0, '2018-03-15 17:24:00', '2018-04-11 19:25:21'),
(26, 1, 9, NULL, '<p>“We only need to do well on one match. We need momentum. We are waiting. If we get some momentum you will see a renewed Bangladesh.”</p>', 1, '2018-03-28 06:45:26', '2018-03-28 06:45:47'),
(27, 1, 9, NULL, '<p>Mahmudullah says the dot balls are the result of doubts in the players’ own minds. Straight losses have affected the team’s self-confidence. But the captain looks ahead to a day when they will regain that confidence.</p>', 1, '2018-03-28 12:46:52', '2018-04-12 23:38:44'),
(29, 2, 7, NULL, '<p>hjgjghj</p>', 0, '2018-04-09 08:10:20', '2018-04-09 08:10:20'),
(30, 2, 9, NULL, '<p>Kanker</p>', 0, '2018-04-09 22:36:57', '2018-04-09 22:36:57'),
(31, 2, 16, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Donec elementum ligula eu sapien consequat eleifend. Donec nec dolor erat, condimentum sagittis sem. Praesent porttitor</p>', 0, '2018-04-10 03:41:13', '2018-04-10 03:41:13'),
(32, 2, 16, NULL, '<p>test</p>', 0, '2018-04-12 19:56:35', '2018-04-12 19:56:35'),
(33, 1, 9, NULL, '<p>test</p>', 0, '2018-04-12 20:36:40', '2018-04-12 20:36:40'),
(34, 3, 10, NULL, '<p>Minus labore aut id officia consectetur nihil unde. Consequatur id modi est. Laboriosam expedita in quis quidem sint repudiandae.</p>', 0, '2018-04-14 12:39:51', '2018-04-14 12:39:51');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `user_id`, `caption`, `image`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'In utgfdgdfg quae ratione nam.', '1.jpg', 1, '2018-03-14 11:24:17', '2018-04-08 14:45:21'),
(3, 1, 'Distinctio amet quia asperiores nam.', '3.jpg', 0, '2018-03-14 11:24:17', '2018-03-14 13:28:58'),
(4, 1, 'Aut maxime error perferendis odit error repellendus cupiditate error.', '4.jpg', 1, '2018-03-14 11:24:17', '2018-03-14 13:28:42'),
(5, 1, 'Architecto animi tempore magnam ab ipsam perferendis cumque.1', '5.jpg', 1, '2018-03-14 11:24:17', '2018-03-14 13:28:14'),
(6, 1, 'Vitae in atque error ut doloribus.', '6.jpg', 0, '2018-03-14 11:24:17', '2018-03-14 13:30:54'),
(7, 1, 'Fugit quidem sed odit qui sunt porro.', '7.jpg', 1, '2018-03-14 11:24:17', '2018-03-14 13:29:11'),
(8, 1, 'Cumque temporibus inventore soluta laborum et.', '8.jpg', 1, '2018-03-14 11:24:17', '2018-03-14 13:28:51'),
(9, 1, 'Perferendis commodi tempora ut ut quia nesciunt.', '9.jpg', 0, '2018-03-14 11:24:17', '2018-03-14 13:29:39'),
(10, 1, 'Id soluta quia est non optio.', '10.jpg', 1, '2018-03-14 11:24:17', '2018-03-14 13:29:25'),
(11, 1, 'Voluptas perspiciatis saepe sed qui voluptatibus eum.', '11.jpg', 1, '2018-03-14 11:24:17', '2018-03-14 13:31:17'),
(12, 1, 'Recusandae voluptatum consequuntur omnis facilis et maiores provident et.', '12.jpg', 1, '2018-03-14 11:24:17', '2018-03-14 13:30:14'),
(13, 1, 'Quo rerum id at.', '13.jpg', 1, '2018-03-14 11:24:17', '2018-03-14 13:30:02'),
(14, 1, 'Autem repellat deserunt doloremque consequatur quo quaerat sunt.', '14.jpg', 0, '2018-03-14 11:24:17', '2018-03-14 13:28:34'),
(15, 1, 'Autem et aperiam est non.', '15.jpg', 0, '2018-03-14 11:24:17', '2018-03-14 13:28:24'),
(16, 1, 'Nawaz', '16.jpg', 1, '2018-03-14 13:20:10', '2018-03-25 05:12:40'),
(17, 1, 'sdfsdf', '17.jpg', 0, '2018-03-14 13:22:34', '2018-04-09 02:35:41'),
(18, 1, 'fgdfgdfg', '18.jpg', 1, '2018-04-08 14:45:42', '2018-04-08 14:45:42');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_02_26_172534_create_categories_table', 1),
(4, '2018_03_06_095213_create_tags_table', 1),
(5, '2018_03_06_103250_create_posts_table', 1),
(6, '2018_03_07_081755_create_post_tag_table', 2),
(7, '2018_03_11_091823_create_subscribers_table', 3),
(8, '2018_03_11_095148_create_settings_table', 3),
(9, '2018_03_14_081014_create_comments_table', 4),
(10, '2018_03_14_100742_create_pages_table', 4),
(11, '2018_03_14_101657_create_galleries_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_featured_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `user_id`, `page_name`, `page_slug`, `page_content`, `page_featured_image`, `meta_title`, `meta_keywords`, `meta_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Earum', 'vitae_provident', 'Minus voluptatibus voluptates sequi vero sunt ducimus. Ea beatae quod sunt est ullam. Vitae soluta quia deleniti eos ad. Libero qui et consequatur maiores eveniet.', '1.jpg', 'Earum', 'voluptas, reiciendis, laudantium', 'Eveniet sint quis officia at commodi nostrum. Sit dolores aut molestiae corporis.', 1, '2018-03-14 11:24:16', '2018-04-12 08:50:44'),
(2, 1, 'Ut', 'et_fuga', 'Eos et perspiciatis sed inventore et sit est. Quae temporibus suscipit id ea eum delectus in. Molestias quia repellat sunt voluptatum. Impedit cum animi voluptatem voluptatem nesciunt velit.', NULL, 'Ut', 'impedit, quia, sed', 'Commodi quia amet enim sint sed saepe. Cupiditate odio ipsum omnis. Laboriosam beatae quia aut et. Sit omnis minus molestiae eos nam tempore id.', 1, '2018-03-14 11:24:16', '2018-04-10 00:35:02'),
(3, 1, 'Quod', 'minus_dignissimos', 'Nisi omnis reiciendis dolore corrupti magni. Vitae neque minima voluptatem ullam rem quod architecto. Quam maxime consequatur labore vero rerum qui. Aspernatur aspernatur aut enim sint et.', '3.jpg', 'Quod', 'et, officia, eos', 'Ea aut unde distinctio aut non. Dicta nemo velit ut velit a veritatis officiis. Sed vel iure aliquid illo eum ab.', 0, '2018-03-14 11:24:17', '2018-03-14 12:53:10'),
(4, 1, 'Et', 'explicabo_vel', 'Cumque soluta eum veniam odio ut voluptatem dolorem porro. Qui aut assumenda unde amet et quia. Inventore perspiciatis veritatis possimus aliquam. Vero eius quo repellat fugiat repudiandae sed nihil.', '4.jpg', 'Et', 'omnis, soluta, sapiente', 'Provident quam hic dignissimos quis est nisi. Fugit eligendi magni natus quis sint quia aut. Sit quo ratione fugit illo occaecati voluptates.', 1, '2018-03-14 11:24:17', '2018-03-14 12:53:02'),
(5, 1, 'Et', 'provident_a', 'Perferendis nemo tempore iure voluptatem rerum dolorum accusantium ad. Incidunt incidunt non sapiente temporibus. Voluptates voluptatem voluptas dolores est dolores perspiciatis saepe.', NULL, 'Et', 'officiis, laborum, numquam', 'Ea vero qui sint numquam rerum nostrum. Consequatur pariatur fugiat ad. Ea consequatur cum cumque rerum. Omnis dolorum ab perferendis.', 0, '2018-03-14 11:24:17', '2018-03-14 11:24:17'),
(6, 1, 'About Us', 'about-us', '<h1 style=\"margin-top: 0px; margin-bottom: 0px; font-size: 28px; font-family: Roboto; line-height: normal; color: rgb(44, 47, 52); padding: 13px 0px 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; letter-spacing: 0.3px;\">Today Computer is the latest technology.</h1><p style=\"margin-bottom: 0px; line-height: 26px; letter-spacing: 0.8px; color: rgb(145, 145, 145); padding-top: 20px; font-family: Roboto, sans-serif;\">If you talk to the engineers and dreamers in Silicon Valley, especially anyone over 35, they\'ll probably admit to being into science fiction. This genre of movies, comic books and novels was huge in the first half of the last century and remained strong through its second half, when most of today\'s engineers were born. That\'s not to say science fiction\'s allure has faded — if anything, the popularity of shows like Westworld and Stranger Things suggests we\'re as fascinated as everblooming</p><p style=\"margin-bottom: 0px; line-height: 26px; letter-spacing: 0.8px; color: rgb(145, 145, 145); padding-top: 20px; font-family: Roboto, sans-serif;\">The Most wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which before with my whole heart. I am alone, and feel the charm of existence in this spot.</p><p style=\"margin-bottom: 0px; line-height: 26px; letter-spacing: 0.8px; color: rgb(145, 145, 145); padding-top: 20px; font-family: Roboto, sans-serif;\">I was born in the latter part of the last century, and like many of my geek friends, was into science fiction at all levels. We loved its heady futuristic ideas and reveled in its high-minded prophesies. But there is one theme in science fiction that always troubled me: when technology runs amok and subverts its creators. Usually when this happens, the story becomes a dramatic puzzle, whose solution involves the protagonists expending tons of creative energy in an effort to either destroy their mutinous creation or contain it.</p><blockquote style=\"padding-top: 32px; margin: 30px auto; font-size: 18px; border: medium none; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: 28px; font-family: Roboto; letter-spacing: 0.4px; max-width: 88%; text-align: center; width: 605.438px; position: relative; color: rgb(44, 47, 52);\">I recently spent time with key execs in the security and cybersecurity space. Perhaps no other area in our digital world underlines the flip side of technological progress. IT execs tell me that security is now about 25% of their IT budget spend.</blockquote><p style=\"margin-bottom: 0px; line-height: 26px; letter-spacing: 0.8px; color: rgb(145, 145, 145); padding-top: 20px; font-family: Roboto, sans-serif;\">I was born in the latter part of the last century, and like many of my geek friends, was into science fiction at all levels. We loved its heady futuristic ideas and reveled in its high-minded prophesies. But there is one theme in science fiction that always troubled me: when technology runs amok and subverts its creators. Usually when this happens, the story becomes a dramatic puzzle, whose solution involves the protagonists expending tons of creative energy in an effort to either destroy their mutinous creation or contain it. I had nightmares for months after I read Mary Shelley\'s Frankenstein mutinous creation or contain it.</p>', '6.jpg', 'Meata Title', 'Meata, Keywords', 'Meta Description', 1, '2018-03-14 12:12:08', '2018-03-14 12:51:32'),
(7, 1, 'sdfsdfsd', 'fsdfsdfsdfsdfsdf', 'sdfsdfdsf', '7.jpg', 'sdfsdf', 'sdfsdf', 'sdfsdfdsf', 1, '2018-03-14 12:14:00', '2018-03-14 12:14:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `post_date` date NOT NULL,
  `post_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_video_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `view_count` int(11) NOT NULL DEFAULT '0',
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `category_id`, `post_date`, `post_title`, `post_slug`, `post_details`, `featured_image`, `youtube_video_url`, `publication_status`, `is_featured`, `view_count`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 1, 10, '1992-12-09', 'Facere quas animi dignissimos minus.', 'ad_qui', '<p>Ullam voluptatem itaque quam non debitis porro. Pariatur dignissimos iste amet expedita praesentium numquam quia. Rerum odio numquam modi esse. <a href=\"https://www.pornliy.com\">free porn</a><br></p>', '1.jpg', 'https://www.youtube.com/watch?v=hK6MJkMzLqE', 1, 1, 20, 'Quidem', 'dolorum, voluptas, ut', '<p>Aut et dolorum velit ea. Magni tenetur autem ratione quam commodi. Distinctio dicta repellendus et nemo numquam omnis.</p>', '2018-03-06 11:10:03', '2018-04-14 20:09:48'),
(2, 1, 2, '2014-02-05', 'Suscipit sapiente natus et asperiores rerum quas iste.', 'neque_a', '<p>Et ut autem non quia voluptates. Et adipisci aut voluptate officiis est eos. A doloremque asperiores quod.</p>', '2.jpg', NULL, 1, 1, 64, 'Veritatis', 'quisquam, aliquid, saepe', '<p>Neque quasi ducimus provident aut ipsum. Numquam consequatur explicabo sit perferendis. Adipisci distinctio sed id. Eos dolorum rerum voluptatem.</p>', '2018-03-06 11:10:03', '2018-04-14 21:11:21'),
(3, 1, 3, '2012-01-11', 'Ut exercitationem sit quaerat saepe quaerat voluptas consequatur.', 'eius_consectetur', '<p>Fugit enim odio cupiditate quo laborum dolorem. Alias eum laudantium ut consectetur. Recusandae aliquid ut consectetur amet.</p>', '3.jpg', NULL, 1, 0, 33, 'Quia', 'quia, quisquam, excepturi', '<p>Debitis ullam deserunt odio in quis. Est aut fuga et odit sit sapiente earum. A id ullam asperiores est modi non.</p>', '2018-03-06 11:10:03', '2018-04-14 20:09:26'),
(4, 3, 8, '2008-04-12', 'Odio cumque nesciunt cupiditate ut.', 'impedit_autem', '<p>Beatae unde dolores deleniti ut rem sunt. Nostrum recusandae cupiditate enim sint fugit id odio. Vero id neque modi sit occaecati deleniti dolores.</p>', '4.jpg', NULL, 1, 1, 35, 'Quas', 'autem, natus, et', '<p>Est corrupti consectetur doloremque et et veritatis. Sit et facilis itaque. Id a tempore labore.</p>', '2018-03-06 11:10:03', '2018-04-14 14:14:30'),
(5, 3, 13, '2016-04-21', 'Et iure enim animi culpa.', 'doloribus_qui', '<p>Saepe distinctio impedit molestiae velit aliquid. Molestiae necessitatibus enim facere aut odit ut facilis.</p>', '5.jpg', NULL, 0, 1, 50, 'Reiciendis', 'veritatis, molestiae, repellat', '<p>Architecto officia nobis dolores non. Distinctio cum magni quos molestiae voluptate qui. Et autem nisi assumenda rerum nihil.</p>', '2018-03-06 11:10:03', '2018-04-19 16:38:29'),
(6, 1, 9, '1977-07-12', 'Magni officia repellat sed rerum in officia.', 'consectetur_suscipit', '<p>Et aliquid eligendi in pariatur libero incidunt. Autem quos laudantium quaerat velit aut tempora. Quis repellat quia quod et.</p>', '6.jpg', NULL, 1, 0, 13, 'Iure', 'voluptate, consequatur, sit', '<p>Et eaque dolor eius optio. Officia perferendis exercitationem sapiente occaecati. Dolore ipsam tenetur dolores optio optio et.</p>', '2018-03-06 11:10:03', '2018-04-14 20:10:56'),
(7, 1, 3, '2002-07-01', 'Vero assumenda quod illo reprehenderit cupiditate.', 'ipsa_ut', '<p>Assumenda excepturi repellendus sit laborum. Cumque harum vero laborum. Vel ad rem earum suscipit.</p>', '7.jpg', NULL, 1, 1, 30, 'Magni', 'magnam, deleniti, eum', '<p>Molestias nisi est deserunt ut sit sint ut. Libero voluptatum a suscipit cupiditate. Et fugiat consequatur fugit sed dolorum.</p>', '2018-03-06 11:10:03', '2018-04-14 20:09:38'),
(8, 3, 3, '1982-11-25', 'Ab velit distinctio id architecto cupiditate doloribus quis sit.', 'vero_deleniti', '<p>Voluptate maiores ut voluptate ipsam quam. Numquam numquam est qui nisi nisi officiis. Deleniti esse cupiditate at accusamus quos sequi possimus. <a href=\"https://prothomalo.com\">Prothomalo</a> </p>', '8.jpg', NULL, 1, 0, 10, 'Tempore', 'ipsam, tempore, impedit', '<p>Aut odio et assumenda iste. Qui inventore omnis labore vel est ut sed. Labore atque earum est mollitia et molestias c</p>', '2018-03-06 11:10:03', '2018-04-14 20:10:51'),
(9, 1, 13, '2017-08-30', 'Eos doloribus distinctio soluta sit fuga.', 'minima_consequuntur', '<p>A single win would quell worries, Bangladesh batsman Tamim Iqbal had said after the first practice session in Sri Lanka.</p>\r\n\r\n<p> But victory eluded the Tigers in their first match.\r\nBangladesh Cricket team T20 Captain Mahmudullah repeated Tamim’s message following Bangladesh\'s defeat at the hands of India.</p>\r\n\r\n<p>Sometimes the bowling falls short, other times it is the batting. The Tigers have yet to fire on all cylinders. Instead, they seem to be making the same mistakes. The tendency of allowing too many dot balls reared its head again on Thursday. The batsmen missed the opportunity to score on nearly half the balls during their 20 overs.</p>\r\n\r\n\r\n\r\n<p>Mahmudullah says the dot balls are the result of doubts in the players’ own minds. Straight losses have affected the team’s self-confidence. But the captain looks ahead to a day when they will regain that confidence.</p>\r\n\r\n<p>A single match could change everything for Bangladesh, Tamim had said. Mahmudullah holds the same belief.</p>\r\n\r\n<p>“We only need to do well on one match. We need momentum. We are waiting. If we get some momentum you will see a renewed Bangladesh.”</p>', '9.jpg', NULL, 1, 1, 136, 'Molestiae', 'quis, exercitationem, dolorem', '<p>Ipsum nihil ea quasi quo ut sequi sunt. Voluptatem excepturi illo vel illo rerum. Dolorum omnis voluptates et quia odio.</p>', '2018-03-06 11:10:04', '2018-04-19 16:29:39'),
(10, 3, 9, '1982-11-04', 'Harum ut inventore eaque molestias ut.', 'sunt_et', '<p>Minus labore aut id officia consectetur nihil unde. Consequatur id modi est. Laboriosam expedita in quis quidem sint repudiandae.</p>', '10.jpg', NULL, 1, 0, 23, 'Quo', 'perferendis, optio, et', '<p>Nobis deleniti itaque culpa dolorem dolores sed. Quis voluptates omnis aut nulla molestias qui. Veritatis sit ut magnam eos ad dignissimos.</p>', '2018-03-06 11:10:04', '2018-04-14 12:39:37'),
(11, 1, 2, '1993-04-14', 'Aliquid vitae itaque id dignissimos reiciendis et.', 'maiores_necessitatibus', '<p>Doloremque sit et et aut. Sapiente dolorem enim eum est numquam aut aut. Ut pariatur ipsum temporibus et. <a href=\"http://www.pornzam.com\">porn</a><br></p>', '11.jpg', NULL, 1, 1, 22, 'Illum', 'dolores, et, sit', '<p>Maxime ullam dignissimos quasi voluptates. Suscipit dolorem accusantium et in molestiae. Accusantium eaque quia rem. Ut ut nisi in nemo culpa.</p>', '2018-03-06 11:10:04', '2018-04-14 20:09:43'),
(12, 1, 11, '1990-06-22', 'Et officia sapiente ad.', 'eum_facere', '<p>Hic architecto aut totam. Id iste odio dolor natus. Libero deleniti sed eius quasi aut ad. Illo assumenda in voluptas quidem.</p>', '12.jpeg', NULL, 1, 0, 9, 'Dolor', 'quod, unde, dolorem', '<p>Quaerat quisquam in delectus sit qui. Doloremque necessitatibus dolor quod temporibus velit.</p>', '2018-03-06 11:10:04', '2018-04-14 20:10:58'),
(13, 3, 2, '2006-02-10', 'In natus dolorem neque atque adipisci ut.', 'voluptatem_ut', '<p>Perferendis iure magni debitis unde. Voluptate aspernatur velit dignissimos sit in et. Voluptatum ab nulla nostrum expedita nesciunt.</p>', '13.jpg', NULL, 1, 1, 40, 'Corporis', 'deleniti, quasi, vel', '<p>Sint hic ab eum. Voluptas voluptas et omnis hic provident. Reprehenderit neque aut architecto sit.</p>', '2018-03-06 11:10:04', '2018-04-14 20:09:34'),
(14, 1, 6, '1976-09-04', 'Nobis voluptates iusto quaerat ut voluptate est.', 'porro_quos', '<p>Fugiat officia nobis error non laboriosam aspernatur ut. Incidunt eveniet omnis nostrum ut. Qui minus adipisci incidunt dolor unde minus aut.</p>', '14.jpg', NULL, 1, 1, 10, 'Error', 'adipisci, qui, impedit', '<p>Alias accusamus sit adipisci ipsam aut nulla earum. Voluptatibus esse consequatur eius eligendi voluptatibus aperiam. Dolore eos et quia cum.</p>', '2018-03-06 11:10:04', '2018-04-14 20:11:00'),
(15, 1, 7, '1972-07-19', 'Nihil ducimus consectetur sunt ratione nihil ipsam voluptatem.', 'modi_ut', '<p>Qui voluptatem odit animi ut quia. Sunt iste et facere sit corrupti maiores quia. Eos eos sapiente magni corrupti.</p>', '15.jpg', NULL, 1, 0, 8, 'Minus', 'autem, repellat, qui', '<p>Est minima quia numquam nobis. Ipsam quia quisquam sequi dicta neque optio. Aperiam nemo occaecati possimus animi amet provident.</p>', '2018-03-06 11:10:04', '2018-04-14 20:11:07'),
(16, 1, 13, '1994-05-26', 'Dolores in sunt debitis eum et sint.', 'sed_officiis', '<p>Consequatur blanditiis at dicta iusto. Accusamus nihil et quod. Provident molestiae expedita amet quia libero.</p>', '16.jpg', NULL, 1, 0, 44, 'Voluptas', 'non, eius, rerum', '<p>Quis nam tempore sed id ab aut minima. Eaque blanditiis rem fugiat dolor. Dolore maxime omnis modi veritatis.</p>', '2018-03-06 11:10:04', '2018-04-14 20:09:41'),
(17, 3, 1, '1983-07-04', 'Reprehenderit consequatur rerum nemo.', 'ea_optio', '<p>Explicabo occaecati deserunt dolorem. Voluptatibus omnis minima aspernatur qui atque. Explicabo occaecati deserunt dolorem. Voluptatibus omnis minima aspernatur qui atque. Explicabo occaecati deserunt dolorem. Voluptatibus omnis minima aspernatur qui atque.<br></p>', '17.jpg', NULL, 1, 0, 10, 'Optio', 'ut, voluptas, consequatur', '<p>Sunt ut minus ullam ut et. Sit reprehenderit consequatur eaque earum et aut consequatur. Quae harum nam voluptate quisquam eum rerum.</p>', '2018-03-06 11:10:04', '2018-04-17 19:24:03'),
(18, 1, 3, '1977-05-06', 'Sed consequuntur et pariatur repellat occaecati.', 'nisi_deserunt', '<p>Consectetur quos et quod excepturi perferendis iure. Earum libero amet sit et. Libero eveniet nobis iure dolores atque exercitationem commodi.</p>', '18.jpg', NULL, 1, 1, 9, 'Fugit', 'assumenda, beatae, voluptate', '<p>Qui vel aperiam ut quia quia. Sed delectus magni quis tenetur. Quo sunt vitae harum eaque cum ducimus.</p>', '2018-03-06 11:10:04', '2018-04-14 20:10:53'),
(19, 3, 1, '1987-05-27', 'Possimus aut praesentium et dolorem.', 'eos_odit', '<p>Pariatur et et qui aut delectus aperiam. Et expedita enim velit quasi cum. Qui aliquam sit fugit iste magni accusantium vitae.</p>', '19.jpg', NULL, 1, 1, 15, 'Molestiae', 'sit, tenetur, cupiditate', '<p>Occaecati est ut quisquam cumque. Aut quidem sit ut esse. Ex laboriosam pariatur quae fuga. Id quia soluta atque quam ab.</p>', '2018-03-06 11:10:04', '2018-04-19 16:29:12'),
(20, 1, 2, '2003-11-11', 'Atque culpa non accusantium repudiandae a.', 'eos_architecto', '<p>Et est ad pariatur. Et ea et provident ut doloribus. Fugit eius consequuntur dolorem ab rerum aspernatur cum. Nemo aspernatur ut sint reiciendis.</p>', '20.jpeg', NULL, 1, 1, 21, 'Tempore', 'est, sit, in', '<p>Voluptate sed neque quia voluptatum. Magnam eos voluptatem numquam similique atque nisi porro sint. Quae voluptas hic qui officia.</p>', '2018-03-06 11:10:04', '2018-04-14 20:09:36'),
(21, 3, 1, '2018-04-17', 'Eos doloribus distinctio soluta sit fuga.', 'minima_consequuntur_a', '<p style=\"color:rgb(145,145,145);\">A single win would quell worries, Bangladesh batsman Tamim Iqbal had said after the first practice session in Sri Lanka.</p>\r\n\r\n<p style=\"color:rgb(145,145,145);\">But victory eluded the Tigers in their first match. Bangladesh Cricket team T20 Captain Mahmudullah repeated Tamim’s message following Bangladesh\'s defeat at the hands of India.</p>\r\n\r\n<p style=\"color:rgb(145,145,145);\">Sometimes the bowling falls short, other times it is the batting. The Tigers have yet to fire on all cylinders. Instead, they seem to be making the same mistakes. The tendency of allowing too many dot balls reared its head again on Thursday. The batsmen missed the opportunity to score on nearly half the balls during their 20 overs.</p>\r\n\r\n<p style=\"color:rgb(145,145,145);\">Mahmudullah says the dot balls are the result of doubts in the players’ own minds. Straight losses have affected the team’s self-confidence. But the captain looks ahead to a day when they will regain that confidence.</p>\r\n\r\n<p style=\"color:rgb(145,145,145);\">A single match could change everything for Bangladesh, Tamim had said. Mahmudullah holds the same belief.</p>\r\n\r\n<p style=\"color:rgb(145,145,145);\">“We only need to do well on one match. We need momentum. We are waiting. If we get some momentum you will see a renewed Bangladesh.”</p>', '21.jpg', 'https://youtube.com/embed/PxhnHGj_SF0', 1, 0, 4, 'Meata Title', 'Meata, Keywords', '<p>A single win would quell worries, Bangladesh batsman Tamim Iqbal had said after the first practice session in Sri Lanka.</p>', '2018-04-17 16:36:52', '2018-04-19 16:28:26');

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE `post_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `post_id`, `tag_id`) VALUES
(7, 8, 4),
(8, 8, 6),
(9, 8, 9),
(10, 8, 13),
(11, 11, 4),
(12, 11, 11),
(13, 11, 14),
(14, 11, 15),
(15, 20, 4),
(16, 20, 5),
(17, 20, 15),
(19, 16, 4),
(20, 16, 12),
(21, 9, 4),
(22, 9, 10),
(23, 9, 11),
(24, 5, 2),
(25, 5, 3),
(26, 5, 4),
(27, 12, 4),
(28, 12, 5),
(29, 12, 6),
(30, 1, 4),
(31, 1, 7),
(32, 1, 8),
(33, 10, 4),
(34, 10, 9),
(35, 10, 10),
(36, 13, 2),
(37, 13, 4),
(38, 13, 14),
(39, 6, 2),
(40, 6, 3),
(41, 6, 4),
(42, 6, 5),
(43, 6, 6),
(44, 6, 7),
(45, 6, 8),
(46, 6, 9),
(47, 6, 10),
(48, 6, 11),
(49, 6, 12),
(50, 6, 13),
(51, 6, 14),
(52, 6, 15),
(54, 15, 3),
(55, 15, 4),
(56, 14, 4),
(57, 14, 5),
(58, 14, 13),
(59, 4, 3),
(60, 4, 4),
(61, 4, 6),
(62, 19, 3),
(63, 19, 4),
(64, 19, 5),
(65, 19, 13),
(66, 17, 3),
(67, 17, 4),
(68, 17, 5),
(69, 18, 3),
(70, 18, 4),
(71, 18, 13),
(72, 2, 4),
(73, 3, 4),
(74, 3, 14),
(75, 7, 2),
(76, 7, 4),
(77, 7, 10),
(78, 21, 3),
(79, 21, 4);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `website_title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_us` text COLLATE utf8mb4_unicode_ci,
  `copyright` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_iframe` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `gallery_meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `website_title`, `logo`, `favicon`, `about_us`, `copyright`, `email`, `phone`, `mobile`, `fax`, `address_line_one`, `address_line_two`, `state`, `city`, `zip`, `country`, `map_iframe`, `facebook`, `twitter`, `google_plus`, `linkedin`, `meta_title`, `meta_keywords`, `meta_description`, `gallery_meta_title`, `gallery_meta_keywords`, `gallery_meta_description`, `created_at`, `updated_at`) VALUES
(1, 'Cluster Coding Blog', 'logo.png', 'favicon.png', 'ClusterCoding is among the pioneers in the Bangladesh to offer quality web services to medium and large sized businesses to compete in today’s digital world. We possess the experience and expertise to help web entrepreneurs reach their customers across the digital space.', 'Copyright 2018 <a href=\"http://clustercoding.com\" target=\"_blank\">Clustercoding</a>, All rights reserved.', 'clustercoding@gmail.com', '+8801717888464', '+8801761913331', '808080', 'House# 83, Road# 16, Sector# 11', 'Uttara', 'Uttara', 'Dhaka', '1230', 'Bangladesh', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2642905.2881059386!2d89.27605108245604!3d23.817470325158617!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30adaaed80e18ba7%3A0xf2d28e0c4e1fc6b!2sBangladesh!5e0!3m2!1sen!2sbd!4v1520764767552\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'https://facebook.com/clustercoding', 'https://twitter.com/cluster_coding', 'https://plus.google.com/+ClusterCoding', 'https://www.linkedin.com/company/clustercoding/', 'Cluster Coding Blog', 'ClusterCoding Blog, Cluster, Coding, Blog', 'ClusterCoding is among the pioneers in the Bangladesh to offer quality web services to medium and large sized businesses to compete in today’s digital world. We possess the experience and expertise to help web entrepreneurs reach their customers across the digital space.', 'Cluster Coding Gallery', 'Cluster, Coding, Gallery', 'Cluster Coding Gallery Cluster Coding Gallery', '2018-03-11 10:37:29', '2018-03-28 04:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'labshire@example.net', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(2, 'gkunze@example.org', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(3, 'rhea12@example.com', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(4, 'bergstrom.melyna@example.com', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(5, 'brandt.greenfelder@example.org', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(6, 'timothy44@example.com', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(7, 'twelch@example.org', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(8, 'ophelia77@example.org', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(9, 'lind.raul@example.com', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(10, 'lurline34@example.net', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(11, 'heaven94@example.org', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(12, 'rveum@example.com', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(13, 'maye21@example.com', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(14, 'brekke.ivah@example.com', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(15, 'bernier.dominic@example.net', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(16, 'shaylee67@example.org', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(17, 'kaleb.padberg@example.org', '2018-03-11 10:37:27', '2018-03-11 10:37:27'),
(18, 'will.rex@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(19, 'mbaumbach@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(20, 'margie47@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(21, 'leannon.burdette@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(22, 'larkin.trenton@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(23, 'waters.mathilde@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(24, 'josefa.goyette@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(25, 'hlemke@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(26, 'myles.reichel@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(27, 'schinner.keara@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(28, 'leopoldo92@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(29, 'ydaugherty@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(30, 'prudence23@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(31, 'padberg.ford@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(32, 'desmond.bednar@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(33, 'gcollier@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(34, 'nschiller@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(35, 'vena.wyman@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(36, 'nolan.maggio@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(37, 'amely.bradtke@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(38, 'yklein@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(39, 'pierce70@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(40, 'becker.roosevelt@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(41, 'rogahn.elvera@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(42, 'walter.lilian@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(43, 'amy.stiedemann@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(44, 'elittle@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(45, 'wmarks@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(46, 'vella65@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(47, 'hellen.mills@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(48, 'gladyce.beier@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(49, 'rframi@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(50, 'hailee.mccullough@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(51, 'wlakin@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(52, 'kertzmann.zita@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(53, 'wanderson@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(54, 'gabriel64@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(55, 'gideon.larson@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(56, 'rwilkinson@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(57, 'lubowitz.kiel@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(58, 'gusikowski.dax@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(59, 'aubree.blick@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(60, 'grant.mills@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(61, 'caleb.koepp@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(62, 'goyette.tatum@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(63, 'weissnat.rosalinda@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(64, 'malachi63@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(65, 'kuhn.michele@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(66, 'rosalia29@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(67, 'xjohnson@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(68, 'osinski.claude@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(69, 'cindy.gaylord@example.net', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(70, 'anastacio24@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(71, 'sandy15@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(72, 'jake.schneider@example.com', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(73, 'flo.jones@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(74, 'krajcik.annabelle@example.org', '2018-03-11 10:37:28', '2018-03-11 10:37:28'),
(75, 'rosina66@example.com', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(76, 'eudora.rice@example.com', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(77, 'aisha76@example.net', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(78, 'jerad00@example.net', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(79, 'twila.mcglynn@example.net', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(80, 'ddaugherty@example.org', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(81, 'moshe56@example.net', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(82, 'juanita33@example.com', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(83, 'rturner@example.org', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(84, 'mohr.cristina@example.com', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(85, 'hermiston.jo@example.com', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(86, 'hamill.jacky@example.net', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(87, 'tcorwin@example.org', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(88, 'parker.hintz@example.com', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(89, 'christiansen.iliana@example.com', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(90, 'pollich.brandon@example.net', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(91, 'kamryn56@example.org', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(92, 'bkutch@example.org', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(93, 'sonya91@example.net', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(94, 'amie81@example.org', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(95, 'aidan.christiansen@example.org', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(96, 'becker.weston@example.com', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(97, 'nthiel@example.org', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(98, 'ghermiston@example.org', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(99, 'tania10@example.com', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(100, 'mhilll@example.com', '2018-03-11 10:37:29', '2018-03-11 10:37:29'),
(103, 'tlc@mail.com', '2018-03-17 12:48:10', '2018-03-17 12:48:10'),
(104, 'mdmahmud@gmail.com', '2018-03-17 12:48:21', '2018-03-17 12:48:21'),
(105, 'shahid2mailbd@gmail.com', '2018-03-17 12:50:25', '2018-03-17 12:50:25'),
(106, 'noyon2nil@gmail.com', '2018-03-17 12:52:54', '2018-03-17 12:52:54'),
(107, 'tlc@mail.comk', '2018-03-17 12:57:35', '2018-03-17 12:57:35'),
(109, 'lawyersconsortium@gmail.com', '2018-04-03 18:24:50', '2018-04-03 18:24:50'),
(110, '123@gmail.com', '2018-04-08 18:17:49', '2018-04-08 18:17:49');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `tag_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `user_id`, `tag_name`, `tag_slug`, `meta_title`, `meta_keywords`, `meta_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(2, 1, 'Delectus', 'et_quia', 'Delectus', 'quos, quia, tenetur', 'Incidunt dolorum sint inventore consequuntur aspernatur laboriosam quidem nulla. Perferendis impedit temporibus fugiat quos.', 1, '2018-03-06 11:10:02', '2018-04-13 04:25:10'),
(3, 1, 'Amet', 'consequuntur_perferendis', 'Amet', 'rerum, omnis, culpa', 'Rerum qui id minima facilis. Et et incidunt officia rem eum. Et corporis in vel reprehenderit magni qui.', 1, '2018-03-06 11:10:02', '2018-03-17 04:37:40'),
(4, 1, 'Repellendus', 'qui_eum', 'Repellendus', 'dolores, inventore, consequuntur', 'Aliquid ut aperiam quaerat. Tempora velit repellendus est. Eligendi reprehenderit quia earum non.', 1, '2018-03-06 11:10:02', '2018-03-06 11:10:02'),
(5, 1, 'Repellat', 'consectetur_vel', 'Repellat', 'iste, ad, temporibus', 'Nobis provident minus officia perspiciatis ut qui. Vel autem sint ad sint. Quam autem qui incidunt repellat quod et nobis.', 1, '2018-03-06 11:10:02', '2018-03-17 04:37:53'),
(6, 1, 'Itaque', 'quia_saepe', 'Itaque', 'aspernatur, odio, vero', 'Sint doloremque soluta et dicta quo quis consequuntur. Iste quo laboriosam voluptate.', 1, '2018-03-06 11:10:02', '2018-03-06 11:10:02'),
(7, 1, 'Consequatur', 'accusantium_sequi', 'Consequatur', 'consequatur, ab, laborum', 'Consectetur illo ut modi ut quisquam nostrum facere voluptas. Eaque ipsum eos dolor veniam. Est quia quisquam repudiandae et qui facere.', 1, '2018-03-06 11:10:02', '2018-03-17 04:37:42'),
(8, 1, 'Fugit', 'expedita_omnis', 'Fugit', 'dolore, veniam, impedit', 'Modi eum reprehenderit et illo. Voluptatem odio quidem necessitatibus voluptate eum quidem et.', 1, '2018-03-06 11:10:02', '2018-03-17 04:37:46'),
(9, 1, 'Est', 'qui_consequuntur', 'Est', 'maiores, autem, dicta', 'Ad sed perferendis non aut. Labore modi enim sequi a et. Ex molestias praesentium ipsam sint. Quo porro in assumenda distinctio.', 1, '2018-03-06 11:10:02', '2018-03-06 11:10:02'),
(10, 1, 'Commodi', 'minima_minus', 'Commodi', 'officia, fuga, inventore', 'Dicta facere non tenetur culpa non dolorum unde qui. Sed laboriosam voluptatem soluta eaque. Dicta culpa quaerat sed omnis modi vero.', 1, '2018-03-06 11:10:02', '2018-03-17 04:37:41'),
(11, 1, 'Rerum', 'quo_quisquam', 'Rerum', 'ut, facilis, molestiae', 'Quam voluptatem et iure modi consectetur ut. Et in nesciunt placeat doloremque.', 1, '2018-03-06 11:10:03', '2018-03-17 04:37:55'),
(12, 1, 'Hic', 'reiciendis_aut', 'Hic', 'architecto, molestias, reiciendis', 'Quas ad ipsum aliquam. Quis dolorem consequatur et ipsam. Voluptatum minima ut ut eligendi expedita veniam non.', 1, '2018-03-06 11:10:03', '2018-03-17 04:37:47'),
(13, 1, 'Et', 'repellat_neque', 'Et', 'expedita, error, omnis', 'Praesentium reiciendis et voluptatum reiciendis neque porro quia. In sit saepe odio nesciunt voluptatem. Labore ut numquam eaque in porro.', 1, '2018-03-06 11:10:03', '2018-03-06 11:10:03'),
(14, 1, 'Et', 'fuga_sint', 'Et', 'earum, veritatis, quod', 'Ea similique excepturi ex nesciunt quam. Totam qui provident quam consectetur corporis. Assumenda voluptatem a ut vitae a ut.', 1, '2018-03-06 11:10:03', '2018-03-17 04:37:45'),
(15, 1, 'Quidem', 'ut_qui', 'Quidem', 'perferendis, sed, aut', 'Aspernatur aut et dolores quis assumenda. Ipsum eum nam qui ipsam. Ut nemo ab sed. Sunt omnis sunt est.', 1, '2018-03-06 11:10:03', '2018-03-17 04:37:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_status` tinyint(4) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `avatar`, `gender`, `phone`, `address`, `facebook`, `twitter`, `google_plus`, `linkedin`, `about`, `role`, `activation_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Bayer DDS', 'admin', 'admin@mail.com', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', '1.png', 'm', '01761913331', 'uttar, dhaka 1230', 'https://www.facebook.com/msnawazbd', 'https://twitter.com/mr_ms_nawaz', 'https://plus.google.com/107488120575807747172', 'https://www.linkedin.com/in/mr-ms-nawaz/', 'This is Nawaz', 'admin', 1, 'N9z7qq4i4cDnpbiOK7b2QYtlXl4qaprbuTqtCvaOi6MR3YKoaN4ymGZEVIB7', '2018-03-06 11:10:00', '2018-03-13 12:07:51'),
(2, 'Magdalena Dickens', 'user', 'user@mail.com', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', '2.png', NULL, NULL, NULL, 'https://www.facebook.com/', 'https://twitter.com/', 'https://plus.google.com/', 'https://www.linkedin.com/', NULL, 'user', 1, 'sYAbetrt7N084j3TuhBqjhL3IuTruSXaJGm4bdlPVR3c4oTCHobEzB4wGyLY', '2018-03-06 11:10:00', '2018-03-13 12:00:01'),
(3, 'Alva Nienow', 'doloribus_laboriosam', 'author@mail.com', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', '3.png', NULL, NULL, NULL, NULL, 'https://twitter.com/', 'https://plus.google.com/', NULL, NULL, 'author', 1, 'KyX3MJX4w2zZqbypAJEVMZAduZJOBtzqYp6HG10qtCoPVyhsMcb1DpNJyIjw', '2018-03-06 11:10:00', '2018-03-13 12:00:37'),
(4, 'Dr. Tracy Bayer DDS', 'voluptas_dolorem', 'alize29@example.com', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, 'https://www.facebook.com/', NULL, 'https://plus.google.com/', NULL, NULL, 'user', 1, 'U05wZNKV3I', '2018-03-06 11:10:00', '2018-03-06 11:10:00'),
(5, 'Ms. Joelle Sporer V', 'reprehenderit_et', 'sherman65@example.com', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, NULL, 'https://twitter.com/', NULL, 'https://www.linkedin.com/', NULL, 'user', 1, 'NQdjpzW3dq', '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(6, 'Liliane Stracke', 'modi_ad', 'dandre.herzog@example.com', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user', 1, 'Z4lXOorlst', '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(7, 'Magdalena Dickens', 'maiores_placeat', 'ydurgan@example.org', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', '7.png', NULL, NULL, NULL, NULL, 'https://twitter.com/', 'https://plus.google.com/', NULL, NULL, 'user', 1, 'lRernN87pqqZpPn7yaG3vAtWQAPKoVINjOnkpUgEhjvcHBRZNj0ZDqLU4gDT', '2018-03-06 11:10:01', '2018-03-13 12:03:23'),
(8, 'Karli Bergstrom', 'inventore_assumenda', 'isabella.osinski@example.org', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, 'https://www.facebook.com/', NULL, NULL, 'https://www.linkedin.com/', NULL, 'user', 1, 'Vic9xqudhs', '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(9, 'Grover Keeling', 'voluptas_asperiores', 'streich.antonia@example.net', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', '9.png', NULL, NULL, NULL, NULL, 'https://twitter.com/', 'https://plus.google.com/', NULL, NULL, 'user', 1, 'dS4nuhyWClGMAmBlaY7yiTsCAWdm0HYqCnLfi8f1Gp50PelE9t5SEIIVlKVc', '2018-03-06 11:10:01', '2018-03-13 12:04:10'),
(10, 'Jacynthe Wehner III', 'molestiae_natus', 'janie34@example.org', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, 'https://www.facebook.com/', NULL, NULL, 'https://www.linkedin.com/', NULL, 'user', 1, 'f9z5YbjVqN', '2018-03-06 11:10:01', '2018-03-06 11:10:01'),
(11, 'Prof. Michaela Ruecker PhD', 'dolore_delectus', 'federico82@example.org', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user', 1, 'TWX78y7w9Q', '2018-03-13 12:10:03', '2018-03-13 12:10:03'),
(12, 'Heath Johns IV', 'voluptas_et', 'ryan.nigel@example.org', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user', 1, 'Hfp97nd7b0', '2018-03-13 12:10:03', '2018-03-13 12:10:03'),
(13, 'Edwardo Hackett MD', 'omnis_saepe', 'adan.batz@example.net', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user', 1, 'GdaixI6MpK', '2018-03-13 12:10:03', '2018-03-13 12:10:03'),
(14, 'Caterina Steuber', 'et_aut', 'neil21@example.org', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user', 1, 'cdzFOEcCYn', '2018-03-13 12:10:03', '2018-03-13 12:10:03'),
(17, 'Karli Fahey', 'possimus_eaque', 'bartoletti.alexane@example.net', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'author', 1, 'sYwdt1Z5FS', '2018-03-13 12:10:04', '2018-04-19 17:08:01'),
(18, 'Gordon Emmerich', 'voluptatem_excepturi', 'diego84@example.com', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user', 1, 'gNE0qleUqX', '2018-03-13 12:10:04', '2018-04-19 17:03:47'),
(19, 'Susana Schuster', 'nobis_asperiores', 'jennie56@example.net', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user', 1, 'ULoCNsWCo0', '2018-03-13 12:10:04', '2018-03-13 12:10:04'),
(20, 'Mr. Alexander Rutherford', 'fugit_aut', 'eborer@example.net', '$2y$10$oWeLVANkG.dkOCsbqynHyue3KL1MUXw6JEnQf2vpaaL.0gCBlYDei', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user', 1, '8VLD0MejQs', '2018-03-13 12:10:04', '2018-03-13 12:10:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_category_slug_unique` (`category_slug`),
  ADD KEY `categories_user_id_index` (`user_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_index` (`user_id`),
  ADD KEY `comments_post_id_index` (`post_id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_user_id_index` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_page_slug_unique` (`page_slug`),
  ADD KEY `pages_user_id_index` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_index` (`user_id`),
  ADD KEY `posts_category_id_index` (`category_id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_tag_post_id_foreign` (`post_id`),
  ADD KEY `post_tag_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subscribers_email_unique` (`email`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_tag_slug_unique` (`tag_slug`),
  ADD KEY `tags_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `galleries_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `post_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
