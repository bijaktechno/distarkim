-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2018 at 11:39 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `distarkim`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `user_id`, `category_name`, `category_slug`, `meta_title`, `meta_keywords`, `meta_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Berita', 'berita', 'Berita', 'Berita', 'Berita', 1, NULL, NULL),
(2, 1, 'Pengumuman', 'pengumuman', 'Pengumuman', 'Pengumuman', 'Pengumuman', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `parent_comment_id` int(10) UNSIGNED DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jenazah`
--

CREATE TABLE `jenazah` (
  `id` int(10) UNSIGNED NOT NULL,
  `makam_id` int(10) UNSIGNED NOT NULL,
  `jenazah_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenazah_nik` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenazah_ahli_waris` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenazah_meninggal_date` date NOT NULL,
  `jenazah_kubur_date` date NOT NULL,
  `jenazah_agama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenazah_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE `kabupaten` (
  `id` int(10) UNSIGNED NOT NULL,
  `propinsi_id` int(10) UNSIGNED NOT NULL,
  `kabupaten_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kabupaten`
--

INSERT INTO `kabupaten` (`id`, `propinsi_id`, `kabupaten_name`, `created_at`, `updated_at`) VALUES
(3214, 32, 'PURWAKARTA', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `kabupaten_id` int(10) UNSIGNED NOT NULL,
  `kecamatan_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `kabupaten_id`, `kecamatan_name`, `created_at`, `updated_at`) VALUES
(3214010, 3214, 'JATILUHUR', NULL, NULL),
(3214011, 3214, 'SUKASARI', NULL, NULL),
(3214020, 3214, 'MANIIS', NULL, NULL),
(3214030, 3214, 'TEGAL WARU', NULL, NULL),
(3214040, 3214, 'PLERED', NULL, NULL),
(3214050, 3214, 'SUKATANI', NULL, NULL),
(3214060, 3214, 'DARANGDAN', NULL, NULL),
(3214070, 3214, 'BOJONG', NULL, NULL),
(3214080, 3214, 'WANAYASA', NULL, NULL),
(3214081, 3214, 'KIARAPEDES', NULL, NULL),
(3214090, 3214, 'PASAWAHAN', NULL, NULL),
(3214091, 3214, 'PONDOK SALAM', NULL, NULL),
(3214100, 3214, 'PURWAKARTA', NULL, NULL),
(3214101, 3214, 'BABAKANCIKAO', NULL, NULL),
(3214110, 3214, 'CAMPAKA', NULL, NULL),
(3214111, 3214, 'CIBATU', NULL, NULL),
(3214112, 3214, 'BUNGURSARI', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id` int(10) UNSIGNED NOT NULL,
  `kecamatan_id` int(10) UNSIGNED NOT NULL,
  `kelurahan_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kelurahan`
--

INSERT INTO `kelurahan` (`id`, `kecamatan_id`, `kelurahan_name`, `created_at`, `updated_at`) VALUES
(321401006, 3214010, 'JATIMEKAR', NULL, NULL),
(321401007, 3214010, 'CIKAOBANDUNG', NULL, NULL),
(321401008, 3214010, 'JATILUHUR', NULL, NULL),
(321401009, 3214010, 'CILEGONG', NULL, NULL),
(321401010, 3214010, 'KEMBANGKUNING', NULL, NULL),
(321401011, 3214010, 'CIBINONG', NULL, NULL),
(321401015, 3214010, 'PARAKANLIMA', NULL, NULL),
(321401016, 3214010, 'CISALADA', NULL, NULL),
(321401017, 3214010, 'MEKARGALIH', NULL, NULL),
(321401018, 3214010, 'BUNDER', NULL, NULL),
(321401101, 3214011, 'PARUNGBANTENG', NULL, NULL),
(321401102, 3214011, 'SUKASARI', NULL, NULL),
(321401103, 3214011, 'CIRIRIP', NULL, NULL),
(321401104, 3214011, 'KERTAMANAH', NULL, NULL),
(321401105, 3214011, 'KUTAMANAH', NULL, NULL),
(321402001, 3214020, 'TEGALDATAR', NULL, NULL),
(321402002, 3214020, 'SINARGALIH', NULL, NULL),
(321402003, 3214020, 'CITAMIANG', NULL, NULL),
(321402004, 3214020, 'CIJATI', NULL, NULL),
(321402005, 3214020, 'GUNUNGKARUNG', NULL, NULL),
(321402006, 3214020, 'PASIRJAMBU', NULL, NULL),
(321402007, 3214020, 'CIRAMAHILIR', NULL, NULL),
(321402008, 3214020, 'SUKAMUKTI', NULL, NULL),
(321403001, 3214030, 'SUKAHAJI', NULL, NULL),
(321403002, 3214030, 'KAROYA', NULL, NULL),
(321403003, 3214030, 'CADASSARI', NULL, NULL),
(321403004, 3214030, 'CADASMEKAR', NULL, NULL),
(321403005, 3214030, 'CITALANG', NULL, NULL),
(321403006, 3214030, 'BATUTUMPANG', NULL, NULL),
(321403007, 3214030, 'TEGALWARU', NULL, NULL),
(321403008, 3214030, 'TEGALSARI', NULL, NULL),
(321403009, 3214030, 'WARUNGJERUK', NULL, NULL),
(321403010, 3214030, 'GALUMPIT', NULL, NULL),
(321403011, 3214030, 'CISARUA', NULL, NULL),
(321403012, 3214030, 'SUKAMULYA', NULL, NULL),
(321403013, 3214030, 'PASANGGRAHAN', NULL, NULL),
(321404001, 3214040, 'RAWASARI', NULL, NULL),
(321404002, 3214040, 'GANDASOLI', NULL, NULL),
(321404003, 3214040, 'GANDAMEKAR', NULL, NULL),
(321404004, 3214040, 'CIBOGOHILIR', NULL, NULL),
(321404005, 3214040, 'PALINGGIHAN', NULL, NULL),
(321404006, 3214040, 'BABAKAN SARI', NULL, NULL),
(321404007, 3214040, 'P L E R E D', NULL, NULL),
(321404008, 3214040, 'SINDANGSARI', NULL, NULL),
(321404009, 3214040, 'CITEKO', NULL, NULL),
(321404010, 3214040, 'CITEKOKALER', NULL, NULL),
(321404011, 3214040, 'LINGGARSARI', NULL, NULL),
(321404012, 3214040, 'PAMOYANAN', NULL, NULL),
(321404013, 3214040, 'LIUNGGUNUNG', NULL, NULL),
(321404014, 3214040, 'ANJUN', NULL, NULL),
(321404015, 3214040, 'CIBOGO GIRANG', NULL, NULL),
(321404016, 3214040, 'SEMPUR', NULL, NULL),
(321405002, 3214050, 'CIANTING', NULL, NULL),
(321405003, 3214050, 'PASIRMUNJUL', NULL, NULL),
(321405004, 3214050, 'CIBODAS', NULL, NULL),
(321405005, 3214050, 'CIANTING UTARA', NULL, NULL),
(321405006, 3214050, 'SUKATANI', NULL, NULL),
(321405007, 3214050, 'MALANGNENGAH', NULL, NULL),
(321405008, 3214050, 'CILALAWI', NULL, NULL),
(321405009, 3214050, 'SUKAMAJU', NULL, NULL),
(321405010, 3214050, 'CIPICUNG', NULL, NULL),
(321405011, 3214050, 'TAJURSINDANG', NULL, NULL),
(321405012, 3214050, 'SINDANGLAYA', NULL, NULL),
(321405013, 3214050, 'PANYINDANGAN', NULL, NULL),
(321405014, 3214050, 'SUKAJAYA', NULL, NULL),
(321405015, 3214050, 'CIJANTUNG', NULL, NULL),
(321406001, 3214060, 'PASIRANGIN', NULL, NULL),
(321406002, 3214060, 'NANGEWER', NULL, NULL),
(321406003, 3214060, 'NEGLASARI', NULL, NULL),
(321406004, 3214060, 'LINGGASARI', NULL, NULL),
(321406005, 3214060, 'SAWIT', NULL, NULL),
(321406006, 3214060, 'SIRNAMANAH', NULL, NULL),
(321406007, 3214060, 'DEPOK', NULL, NULL),
(321406008, 3214060, 'LEGOKSARI', NULL, NULL),
(321406009, 3214060, 'MEKARSARI', NULL, NULL),
(321406012, 3214060, 'GUNUNGHEJO', NULL, NULL),
(321406013, 3214060, 'DARANGDAN', NULL, NULL),
(321406014, 3214060, 'SADARKARYA', NULL, NULL),
(321406015, 3214060, 'LINGGAMUKTI', NULL, NULL),
(321406016, 3214060, 'CILINGGA', NULL, NULL),
(321406017, 3214060, 'NAGRAK', NULL, NULL),
(321407001, 3214070, 'CIBINGBIN', NULL, NULL),
(321407002, 3214070, 'BOJONG TIMUR', NULL, NULL),
(321407003, 3214070, 'PASANGGRAHAN', NULL, NULL),
(321407004, 3214070, 'CIHANJAWAR', NULL, NULL),
(321407005, 3214070, 'CIKERIS', NULL, NULL),
(321407006, 3214070, 'BOJONG BARAT', NULL, NULL),
(321407007, 3214070, 'PANGKALAN', NULL, NULL),
(321407008, 3214070, 'SUKAMANAH', NULL, NULL),
(321407009, 3214070, 'PAWENANG', NULL, NULL),
(321407010, 3214070, 'SINDANGSARI', NULL, NULL),
(321407011, 3214070, 'SINDANGPANON', NULL, NULL),
(321407012, 3214070, 'CIPEUNDEUY', NULL, NULL),
(321407013, 3214070, 'CILEUNCA', NULL, NULL),
(321407014, 3214070, 'KERTASARI', NULL, NULL),
(321408001, 3214080, 'NANGERANG', NULL, NULL),
(321408002, 3214080, 'SIMPANG', NULL, NULL),
(321408003, 3214080, 'SAKAMBANG', NULL, NULL),
(321408004, 3214080, 'NAGROG', NULL, NULL),
(321408005, 3214080, 'CIBUNTU', NULL, NULL),
(321408006, 3214080, 'SUMURUGUL', NULL, NULL),
(321408007, 3214080, 'RAHARJA', NULL, NULL),
(321408008, 3214080, 'WANAYASA', NULL, NULL),
(321408009, 3214080, 'BABAKAN', NULL, NULL),
(321408016, 3214080, 'WANASARI', NULL, NULL),
(321408017, 3214080, 'LEGOKHUNI', NULL, NULL),
(321408018, 3214080, 'CIAWI', NULL, NULL),
(321408019, 3214080, 'SUKADAMI', NULL, NULL),
(321408020, 3214080, 'TARINGGUL TONGGOH', NULL, NULL),
(321408021, 3214080, 'TARINGGUL TENGAH', NULL, NULL),
(321408101, 3214081, 'PUSAKAMULYA', NULL, NULL),
(321408102, 3214081, 'PARAKAN GAROKGEK', NULL, NULL),
(321408103, 3214081, 'CIRACAS', NULL, NULL),
(321408104, 3214081, 'KIARAPEDES', NULL, NULL),
(321408105, 3214081, 'CIBEBER', NULL, NULL),
(321408106, 3214081, 'SUMBERSARI', NULL, NULL),
(321408107, 3214081, 'MEKARJAYA', NULL, NULL),
(321408108, 3214081, 'MARGALUYU', NULL, NULL),
(321408109, 3214081, 'GARDU', NULL, NULL),
(321408110, 3214081, 'TARINGGUL LANDEUH', NULL, NULL),
(321409009, 3214090, 'CIHERANG', NULL, NULL),
(321409010, 3214090, 'CIDAHU', NULL, NULL),
(321409011, 3214090, 'PASAWAHAN ANYAR', NULL, NULL),
(321409012, 3214090, 'PASAWAHANKIDUL', NULL, NULL),
(321409013, 3214090, 'SAWAH KULON', NULL, NULL),
(321409014, 3214090, 'KERTAJAYA', NULL, NULL),
(321409015, 3214090, 'LEBAKANYAR', NULL, NULL),
(321409016, 3214090, 'CIHUNI', NULL, NULL),
(321409017, 3214090, 'WARUNGKADU', NULL, NULL),
(321409018, 3214090, 'SELAAWI', NULL, NULL),
(321409019, 3214090, 'MARGASARI', NULL, NULL),
(321409020, 3214090, 'PASAWAHAN', NULL, NULL),
(321409101, 3214091, 'BUNGUR JAYA', NULL, NULL),
(321409102, 3214091, 'PONDOKBUNGUR', NULL, NULL),
(321409103, 3214091, 'SALEM', NULL, NULL),
(321409104, 3214091, 'GALUDRA', NULL, NULL),
(321409105, 3214091, 'SUKAJADI', NULL, NULL),
(321409106, 3214091, 'TANJUNGSARI', NULL, NULL),
(321409107, 3214091, 'SALAM JAYA', NULL, NULL),
(321409108, 3214091, 'SITU', NULL, NULL),
(321409109, 3214091, 'PARAKANSALAM', NULL, NULL),
(321409110, 3214091, 'SALAM MULYA', NULL, NULL),
(321409111, 3214091, 'GURUDUG', NULL, NULL),
(321410005, 3214100, 'SINDANGKASIH', NULL, NULL),
(321410006, 3214100, 'NAGERI KIDUL', NULL, NULL),
(321410007, 3214100, 'NAGERI TENGAH', NULL, NULL),
(321410008, 3214100, 'CIPAISAN', NULL, NULL),
(321410009, 3214100, 'NAGERI KALER', NULL, NULL),
(321410010, 3214100, 'TEGALMUNJUL', NULL, NULL),
(321410011, 3214100, 'CITALANG', NULL, NULL),
(321410012, 3214100, 'MUNJULJAYA', NULL, NULL),
(321410013, 3214100, 'CISEUREUH', NULL, NULL),
(321410016, 3214100, 'PURWAMEKAR', NULL, NULL),
(321410101, 3214101, 'MARACANG', NULL, NULL),
(321410102, 3214101, 'CIWARENG', NULL, NULL),
(321410103, 3214101, 'MULYAMEKAR', NULL, NULL),
(321410104, 3214101, 'CIGELAM', NULL, NULL),
(321410105, 3214101, 'BABAKANCIKAO', NULL, NULL),
(321410106, 3214101, 'KADUMEKAR', NULL, NULL),
(321410107, 3214101, 'HEGARMANAH', NULL, NULL),
(321410108, 3214101, 'CICADAS', NULL, NULL),
(321410109, 3214101, 'CILANGKAP', NULL, NULL),
(321411001, 3214110, 'CIRENDE', NULL, NULL),
(321411010, 3214110, 'BENTENG', NULL, NULL),
(321411011, 3214110, 'CAMPAKA', NULL, NULL),
(321411012, 3214110, 'CAMPAKASARI', NULL, NULL),
(321411023, 3214110, 'CIJUNTI', NULL, NULL),
(321411024, 3214110, 'CISAAT', NULL, NULL),
(321411025, 3214110, 'CIMAHI', NULL, NULL),
(321411026, 3214110, 'CIKUMPAY', NULL, NULL),
(321411027, 3214110, 'CIJAYA', NULL, NULL),
(321411028, 3214110, 'KERTAMUKTI', NULL, NULL),
(321411101, 3214111, 'WANAWALI', NULL, NULL),
(321411102, 3214111, 'CIKADU', NULL, NULL),
(321411103, 3214111, 'CIBUKAMANAH', NULL, NULL),
(321411104, 3214111, 'CIRANGKONG', NULL, NULL),
(321411105, 3214111, 'CIPANCUR', NULL, NULL),
(321411106, 3214111, 'CIPINANG', NULL, NULL),
(321411107, 3214111, 'CIPARUNGSARI', NULL, NULL),
(321411108, 3214111, 'KARYAMEKAR', NULL, NULL),
(321411109, 3214111, 'CIBATU', NULL, NULL),
(321411110, 3214111, 'CILANDAK', NULL, NULL),
(321411201, 3214112, 'CIWANGI', NULL, NULL),
(321411202, 3214112, 'CIBENING', NULL, NULL),
(321411203, 3214112, 'BUNGURSARI', NULL, NULL),
(321411204, 3214112, 'CIBUNGUR', NULL, NULL),
(321411205, 3214112, 'DANGDEUR', NULL, NULL),
(321411206, 3214112, 'WANAKERTA', NULL, NULL),
(321411207, 3214112, 'CINANGKA', NULL, NULL),
(321411208, 3214112, 'CIKOPO', NULL, NULL),
(321411209, 3214112, 'KARANGMUKTI', NULL, NULL),
(321411210, 3214112, 'CIBODAS', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `makam`
--

CREATE TABLE `makam` (
  `id` int(10) UNSIGNED NOT NULL,
  `kelurahan_id` int(10) UNSIGNED NOT NULL,
  `makam_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `makam_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(267, '2014_10_12_000000_create_users_table', 1),
(268, '2014_10_12_100000_create_password_resets_table', 1),
(269, '2018_02_26_172534_create_categories_table', 1),
(270, '2018_03_06_095213_create_tags_table', 1),
(271, '2018_03_06_103250_create_posts_table', 1),
(272, '2018_03_07_081755_create_post_tag_table', 1),
(273, '2018_03_11_091823_create_subscribers_table', 1),
(274, '2018_03_11_095148_create_settings_table', 1),
(275, '2018_03_14_081014_create_comments_table', 1),
(276, '2018_03_14_100742_create_pages_table', 1),
(277, '2018_03_14_100743_create_page_files_table', 1),
(278, '2018_03_14_101657_create_galleries_table', 1),
(279, '2018_11_03_201824_create_kabupaten_table', 1),
(280, '2018_11_03_202751_create_kecamatan_table', 1),
(281, '2018_11_03_202808_create_kelurahan_table', 1),
(282, '2018_11_03_202839_create_makam_table', 1),
(283, '2018_11_03_202902_create_jenazah_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_featured_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_type` int(10) UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `user_id`, `page_name`, `page_slug`, `page_content`, `page_featured_image`, `page_type`, `meta_title`, `meta_keywords`, `meta_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Struktur Organisasi', 'struktur_organisasi', '<p>Struktur Organisasi<br></p>', '1.jpg', 0, 'Struktur Organisasi', 'Struktur Organisasi', 'Struktur Organisasi', 1, NULL, NULL),
(2, 1, 'Tugas dan Fungsi', 'tugas_dan_fungsi', '<p class=\"fs14 col686868\" style=\"margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;\">Tugas dan fungsi Dinas Tata Ruang diatur dalam&nbsp;<br>Peraturan Bupati No.148 Tahun 2016 tentang Kedudukan, Susunan Organisasi, Tugas dan Fungsi Perangkat&nbsp;<br>Daerah sebagai berikut :</p><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><h6 style=\"margin-top: 0px; margin-bottom: 0.5rem; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; line-height: 1.2; color: rgb(33, 37, 41); font-size: 1rem;\">Tugas Pokok :</h6><p class=\"fs14 col686868\" style=\"margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;\">Melaksanakan sebagian urusan pemerintahan daerah di bidang pekerjaan umum lingkup keciptakaryaan, penataan ruang, perumahan dan permukiman berdasarkan asas otonomi dan tugas pembantuan.</p><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><h6 style=\"margin-top: 0px; margin-bottom: 0.5rem; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; line-height: 1.2; color: rgb(33, 37, 41); font-size: 1rem;\">Fungsi :</h6><p style=\"margin-bottom: 1rem; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><ul class=\"fs14 col686868\" style=\"margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;\"><li>Perumusan kebijakan teknis bidang keciptakaryaan, penataan ruang, dan perumahan;</li><li>Penyelenggaraan sebagian urusan pemerintahan dan pelayanan umum di bidang pekerjaan umum lingkup keciptakaryaan, penataan ruang dan perumahan;</li><li>Pembinaan dan pelaksanaan tugas yang meliputi bidang tata bangunan, pembinaan, pengawasan dan pengendalian, perumahan, permukiman, dan tata ruang.</li><li>Pelaksanaan pelayanan teknis administratif dinas</li><li>Pelaksanaan tugas lain yang diberikan oleh bupati sesuai dengan tugas dan fungsinya.</li></ul>', '', 0, 'Tugas dan Fungsi', 'Tugas dan Fungsi', 'Tugas dan Fungsi', 1, NULL, NULL),
(3, 1, 'Visi dan Misi', 'visi_misi', '<h5 class=\"col844D28\" style=\"margin-top: 0px; margin-bottom: 0.5rem; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; line-height: 1.2; color: rgb(132, 77, 40); font-size: 1.25rem;\">Visi</h5><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><p class=\"col009141\" style=\"margin-bottom: 1rem; color: rgb(0, 145, 65); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"><i>\"Terwujudnya Tata Ruang dan Bangunan serta Sarana dan Prasarana Perumahan dan Permukiman yang Berkarakter dan Berwawasan Lingkungan\".</i></p><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><p class=\"fs14 col686868\" style=\"margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;\">Visi Dinas Tata Ruang dan Permukiman Kabupaten Purwakarta merupakan penjabaran dari Visi Pemerintah&nbsp;<br>Kabupaten Purwakarta dan Misi ke-2(dua) Kabupaten Purwakarta sebagaimana telah tertuang dalam RPJMD&nbsp;<br>Kabupaten Purwakarta Tahun 2013-2018</p><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><p class=\"fs14 col686868\" style=\"margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;\">Visi Pembangunan Jangka Menengah Kabupaten Purwakarta Tahun 2013-2018 adalah&nbsp;<br><span style=\"font-weight: bolder;\">\"PURWAKARTA BERKARAKTER\"</span></p><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><h5 class=\"col844D28\" style=\"margin-top: 0px; margin-bottom: 0.5rem; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; line-height: 1.2; color: rgb(132, 77, 40); font-size: 1.25rem;\">Misi</h5><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><p class=\"fs14 col686868\" style=\"margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;\">Dinas Tata Ruang dan Permukiman Kabupaten Purwakarta menetapkan Misi sebagai berikut :</p><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><p class=\"fs14 col686868\" style=\"margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;\">1. Mewujudkan Sumber Daya Aparatur Yang Profesional Dalam Rangka Meningkatkan Pelayanan Kepada Masyarakat.</p><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><p class=\"fs14 col686868\" style=\"margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;\">2. Meningkatkan Sarana Dan Prasarana Perumahan Dan Permukiman Dalam Rangka Terpenuhinya Kebutuhan Dasar Masyarakat Dan Lingkungan Yang Bersih, Nyaman Dan Aman.</p><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><p class=\"fs14 col686868\" style=\"margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;\">3. Mewujudkan Pelayanan Infrastruktur Wilayah Di Bidang Penyediaan Prasarana dan Sarana Air Bersih.</p><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><p class=\"fs14 col686868\" style=\"margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;\">4. Mewujudkan Penyelenggaraan Penataan Ruang Secara Transparan, Efektif Dan Partisipatif Melalui Kegiatan Pengaturan, Pembinaan, Pelaksanaan Dan Pengawasan Agar Terwujud Penataan Ruang Yang Aman, Nyaman, Produktif Dan Berkelanjutan.</p><p><br style=\"color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"></p><p class=\"fs14 col686868\" style=\"margin-bottom: 1rem; color: rgb(104, 104, 104); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;\">5. Mewujudkan Tata Kelola Pertanahan Untuk Kepentingan Pemerintah Daerah Dan Pembangunan Fasilitas Pubik Agar Terwujud Tertib Administrasi Pertanahan.</p><div><br></div>', '', 0, 'Visi dan Misi', 'Visi dan Misi', 'Visi dan Misi', 1, NULL, NULL),
(4, 1, 'Tupoksi', 'tupoksi', '<p>lorem ipsum</p>', '', 1, 'Tupoksi', 'Tupoksi', 'Tupoksi', 1, NULL, NULL),
(5, 1, 'Data Taman', 'data_taman', '<p>lorem ipsum</p>', '', 1, 'Data Taman', 'Data Taman', 'Data Taman', 1, NULL, NULL),
(6, 1, 'IMB', 'imb', '<p>lorem ipsum</p>', '', 1, 'IMB', 'IMB', 'IMB', 1, NULL, NULL),
(7, 1, 'SK SOP Pemakaman', 'sop_pemakaman', '<p>lorem ipsum</p>', '', 1, 'SK SOP Pemakaman', 'SK SOP Pemakaman', 'SK SOP Pemakaman', 1, NULL, NULL),
(8, 1, 'SOP Registrasi Pemakaman', 'sop_registrasi_pemakaman', '<p>lorem ipsum</p>', '', 1, 'SOP Registrasi Pemakaman', 'SOP Registrasi Pemakaman', 'SOP Registrasi Pemakaman', 1, NULL, NULL),
(9, 1, 'Peraturan', 'peraturan', '<p>lorem ipsum</p>', '', 1, 'Peraturan', 'Peraturan', 'Peraturan', 1, NULL, NULL),
(10, 1, 'Layanan', 'layanan', '<p>lorem ipsum</p>', '', 1, 'Layanan', 'Layanan', 'Layanan', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page_files`
--

CREATE TABLE `page_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `file_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_files`
--

INSERT INTO `page_files` (`id`, `page_id`, `file_title`, `file_path`, `created_at`, `updated_at`) VALUES
(1, 5, 'data-taman.pdf', '5.data-taman.pdf', NULL, NULL),
(2, 5, '1Renstra-2017-2018.pdf', '5.1Renstra-2017-2018.pdf', NULL, NULL),
(3, 5, '1Renja-2018.pdf', '5.1Renja-2018.pdf', NULL, NULL),
(4, 5, '1IKU-Distarkim-2017-2018.pdf', '5.1IKU-Distarkim-2017-2018.pdf', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `post_date` date NOT NULL,
  `post_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_video_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `view_count` int(11) NOT NULL DEFAULT '0',
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE `post_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `website_title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_us` text COLLATE utf8mb4_unicode_ci,
  `copyright` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_iframe` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `gallery_meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `website_title`, `logo`, `favicon`, `about_us`, `copyright`, `email`, `phone`, `mobile`, `fax`, `address_line_one`, `address_line_two`, `state`, `city`, `zip`, `country`, `map_iframe`, `facebook`, `twitter`, `google_plus`, `linkedin`, `meta_title`, `meta_keywords`, `meta_description`, `gallery_meta_title`, `gallery_meta_keywords`, `gallery_meta_description`, `created_at`, `updated_at`) VALUES
(1, 'DISTARKIM', 'logo.png', 'favicon.png', 'Visi Dinas Tata Ruang dan Permukiman Kabupaten Purwakarta merupakan penjabaran dari Visi Pemerintah Kabupaten Purwakarta dan Misi ke-2(dua) Kabupaten Purwakarta sebagaimana telah tertuang dalam RPJMD Kabupaten Purwakarta Tahun 2013-2018.', 'Copyright 2018 <a href=\"http://distarkim.purwakartakab.go.id/\" target=\"_blank\">Distarkim</a>, All rights reserved.', 'distarkim@gmail.com', '+8801717888464', '+8801761913331', '808080', 'House# 83, Road# 16, Sector# 11', 'Purwakarta', 'Purwakarta', 'Purwakarta', '12345', 'Indonesia', '\n		<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1981.8586994225045!2d107.44609445791026!3d-6.55731479881435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e690e4ef9d22ce7%3A0x6b12d62f529446a7!2sDinas+Tata+Ruang+dan+Permukiman+Kabupaten+Purwakarta!5e0!3m2!1sid!2sid!4v1541513363710\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'https://facebook.com/distarkimpurwakarta', 'https://twitter.com/distarkimpwk', '', '', 'DISTARKIM', 'DISTARKIM', 'DISTARKIM.', 'DISTARKIM', 'DISTARKIM', 'DISTARKIM.', '2018-11-11 09:37:27', '2018-11-11 09:37:27');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `tag_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `publication_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_status` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `avatar`, `gender`, `phone`, `address`, `facebook`, `twitter`, `google_plus`, `linkedin`, `about`, `role`, `activation_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'admin@mail.com', '$2y$10$PE/vlxzr3N4kwSvX/o.EGeIw28/4Niz/OTxktAozcyerJQlm6dL6i', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', 0, 'PmRM3a9F3a', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_category_slug_unique` (`category_slug`),
  ADD KEY `categories_user_id_index` (`user_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_index` (`user_id`),
  ADD KEY `comments_post_id_index` (`post_id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_user_id_index` (`user_id`);

--
-- Indexes for table `jenazah`
--
ALTER TABLE `jenazah`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jenazah_makam_id_index` (`makam_id`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kabupaten_propinsi_id_index` (`propinsi_id`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kecamatan_kabupaten_id_index` (`kabupaten_id`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kelurahan_kecamatan_id_index` (`kecamatan_id`);

--
-- Indexes for table `makam`
--
ALTER TABLE `makam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `makam_kelurahan_id_index` (`kelurahan_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_page_slug_unique` (`page_slug`),
  ADD KEY `pages_user_id_index` (`user_id`);

--
-- Indexes for table `page_files`
--
ALTER TABLE `page_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_files_page_id_index` (`page_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_index` (`user_id`),
  ADD KEY `posts_category_id_index` (`category_id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_tag_post_id_foreign` (`post_id`),
  ADD KEY `post_tag_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subscribers_email_unique` (`email`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_tag_slug_unique` (`tag_slug`),
  ADD KEY `tags_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jenazah`
--
ALTER TABLE `jenazah`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kabupaten`
--
ALTER TABLE `kabupaten`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3215;
--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3214113;
--
-- AUTO_INCREMENT for table `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=321411211;
--
-- AUTO_INCREMENT for table `makam`
--
ALTER TABLE `makam`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `page_files`
--
ALTER TABLE `page_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `galleries_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `jenazah`
--
ALTER TABLE `jenazah`
  ADD CONSTRAINT `jenazah_makam_id_foreign` FOREIGN KEY (`makam_id`) REFERENCES `makam` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD CONSTRAINT `kecamatan_kabupaten_id_foreign` FOREIGN KEY (`kabupaten_id`) REFERENCES `kabupaten` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD CONSTRAINT `kelurahan_kecamatan_id_foreign` FOREIGN KEY (`kecamatan_id`) REFERENCES `kecamatan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `makam`
--
ALTER TABLE `makam`
  ADD CONSTRAINT `makam_kelurahan_id_foreign` FOREIGN KEY (`kelurahan_id`) REFERENCES `kelurahan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `page_files`
--
ALTER TABLE `page_files`
  ADD CONSTRAINT `page_files_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `post_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
