@extends('admin.layouts.app')
@section('title', 'Pelayanan Registrasi TPU')


@section('style')
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css"> -->

	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/dataTables.bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/buttons.dataTables.min.css')}}">
	<link rel="stylesheet" href="{{ asset('public/admin/css/bootstrap-datepicker.min.css') }}">
	@endsection

	@section('content')
	<!-- Page header -->
	<section class="content-header">
		<h1>
			Data Pelayanan Registrasi TPU
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('admin.dashboardRoute') }}"><i class="fa fa-home"></i> Dashboard</a></li>
			<li class="active">Data Pelayanan Registrasi TPU</li>
		</ol>
	</section>
	<!-- /.page header -->

	<!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Manage Data Pelayanan Registrasi TPU</h3>

				<div class="box-tools">
					<button type="button" class="btn btn-info btn-sm btn-flat add-button"><i class="fa fa-plus"></i> Add Data Pelayanan Registrasi TPU</button>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div style="width: 100%; padding-left: -10px;">
					<div class="table-responsive">
						<table id="jenazah-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0">
							<thead>
								<tr>
									<th>Nama Makam</th>
									<th>Nama Jenazah</th>
									<th>NIK</th>
									<th>Tanggal Meninggal</th>
									<th>Created At</th>
									<th>Updated At</th>
									<th width="10%">Publication Status</th>
									<th width="7%">Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
			</div>
		</div>
	</section>
	<!-- /.main content -->

	<!-- add jenazah modal -->
	<div id="add-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							<span class="fa-stack fa-sm">
								<i class="fa fa-square-o fa-stack-2x"></i>
								<i class="fa fa-plus fa-stack-1x"></i>
							</span>
							Add Data Pelayanan Registrasi TPU
						</h4>
					</div>
					<form role="form" id="jenazah_add_form" method="post">
						{{ csrf_field() }}
						<div class="modal-body">
							<div class="form-group">
								<label for="makam_id">Makam</label>
								<select name="makam_id" class="form-control" id="makam_id">
									<option value="" selected disabled>Select One</option>
									@foreach($makam as $makam_value)
									<option value="{{ $makam_value->id }}">{{ $makam_value->makam_name}}</option>
									@endforeach
								</select>
								<span class="text-danger" id="makam-id-error"></span>
							</div>


							<div class="form-group">
								<label for="jenazah_name">Nama Jenazah</label>
								<input type="text" name="jenazah_name" class="form-control" id="jenazah_name" value="{{ old('jenazah_name') }}" placeholder="ex: nama jenazah">
								<span class="text-danger" id="jenazah-name-error"></span>
							</div>

							<div class="form-group">
								<label for="jenazah_nik">NIK</label>
								<input type="number" name="jenazah_nik" class="form-control" id="jenazah_nik" value="{{ old('jenazah_nik') }}" placeholder="ex: 12345678910">
								<span class="text-danger" id="jenazah-nik-error"></span>
							</div>
							<div class="form-group">
								<label for="jenazah_ahli_waris">Ahli Waris</label>
								<input type="text" name="jenazah_ahli_waris" class="form-control" id="jenazah_ahli_waris" value="{{ old('jenazah_ahli_waris') }}" placeholder="ex: ahli waris">
								<span class="text-danger" id="jenazah-ahli-waris-error"></span>
							</div>

							<div class="form-group">
								<label for="jenazah_meninggal_date">Tanggal Meninggal</label>
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" name="jenazah_meninggal_date" class="form-control pull-right" id="jenazah_meninggal_date">
								</div>
								<span class="text-danger" id="jenazah-meninggal-date-error"></span>
							</div>

							<div class="form-group">
								<label for="jenazah_kubur_date">Tanggal Kubur</label>
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" name="jenazah_kubur_date" class="form-control pull-right" id="jenazah_kubur_date">
								</div>
								<span class="text-danger" id="jenazah-kubur-date-error"></span>
							</div>


							
							<div class="form-group">
								<label>Agama</label>
								<select class="form-control" name="jenazah_agama" id="jenazah_agama">
									<option selected disabled>Select One</option>
									<option value="Islam">Islam</option>
									<option value="Katolik">Katolik</option>
									<option value="Protestan">Protestan</option>
									<option value="Hindu">Hindu</option>
									<option value="BUdha">BUdha</option>
									<option value="Konghucu">Konghucu</option>
									<option value="Lainnya">Lainnya</option>
								</select>
								<span class="text-danger" id="jenazah-agama-error"></span>
							</div>

							<div class="form-group">
								<label>Status</label>
								<select class="form-control" name="jenazah_status" id="jenazah_status">
									<option selected disabled>Select One</option>
									<option value="Single">Single</option>
									<option value="Menikah">Menikah</option>
									<option value="Janda">Janda</option>
									<option value="Duda">Duda</option>
								</select>
								<span class="text-danger" id="jenazah-status-error"></span>
							</div>

							<div class="form-group">
								<label>Publication Status</label>
								<select class="form-control" name="publication_status" id="publication_status">
									<option selected disabled>Select One</option>
									<option value="1">Published</option>
									<option value="0">Unpublished</option>
								</select>
								<span class="text-danger" id="publication-status-error"></span>
							</div>


						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-info btn-flat" id="store-button">Save changes</button>
						</div>
					</form>

				</div>
			</div>
		</div>
		<!-- /.add jenazah modal -->

		<!-- view jenazah modal -->
		<div id="view-modal" class="modal fade bs-example-modal-lg print-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="btn-group pull-right no-print">
							<div class="btn-group">
								<button class="tip btn btn-default btn-flat btn-sm" id="print-button" data-toggle="tooltip" data-original-title="Print">
									<i class="fa fa-print"></i>
									<span class="hidden-sm hidden-xs"></span>
								</button>
							</div>
							<div class="btn-group">
								<button class="tip btn btn-default btn-flat btn-sm" data-toggle="tooltip" data-original-title="Close" data-dismiss="modal" aria-label="Close">
									<i class="fa fa-remove"></i>
									<span class="hidden-sm hidden-xs"></span>
								</button>
							</div>
						</div>
						<h4 class="modal-title" id="view-jenazah-name"></h4>
					</div>
					<div class="modal-body">
						<table class="table table-bordered table-striped">
							<tbody>
								<tr>
									<td>Makam</td>
									<td id="view-makam-id"></td>
								</tr>
								<tr>
									<td>NIK</td>
									<td id="view-jenazah-nik"></td>
								</tr>
								<tr>
									<td>Ahli Waris</td>
									<td id="view-jenazah-ahli-waris"></td>
								</tr>
								<tr>
									<td>Tanggal Meninggal</td>
									<td id="view-jenazah-meninggal-date"></td>
								</tr>
								<tr>
									<td>Tanggal Kubur</td>
									<td id="view-jenazah-kubur-date"></td>
								</tr>
								<tr>
									<td>Agama</td>
									<td id="view-jenazah-agama"></td>
								</tr>
								<tr>
									<td>Status</td>
									<td id="view-jenazah-status"></td>
								</tr>
								<tr>
									<td>Publication Status</td>
									<td id="view-publication-status"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer no-print">
						<button type="button" class="btn btn-default btn-flat" data-dismiss="modal" aria-label="Close">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- /.view jenazah modal -->

		<!-- delete jenazah modal -->
		<div id="delete-modal" class="modal modal-danger fade" id="modal-danger">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">
								<span class="fa-stack fa-sm">
									<i class="fa fa-square-o fa-stack-2x"></i>
									<i class="fa fa-trash fa-stack-1x"></i>
								</span>
								Are you sure want to delete this ?
							</h4>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
							<form method="post" role="form" id="delete_form">
								{{csrf_field()}}
								{{method_field('DELETE')}}
								<button type="submit" class="btn btn-outline">Delete</button>
							</form>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
		</div>
			<!-- /.delete jenazah modal -->


		<!-- edit jenazah modal -->
		<div id="edit-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">
								<span class="fa-stack fa-sm">
									<i class="fa fa-square-o fa-stack-2x"></i>
									<i class="fa fa-edit fa-stack-1x"></i>
								</span>
								Edit Data Pelayanan Registrasi TPU
							</h4>
						</div>
						<form role="form" id="jenazah_edit_form" method="post">
							{{method_field('PATCH')}}
							{{csrf_field()}}
							<input type="hidden" name="jenazah_id" id="edit-jenazah-id">
							<div class="modal-body">

								<div class="form-group">
									<label for="makam_id">Makam</label>
									<select name="makam_id" class="form-control" id="edit-makam-id">
										<option value="" selected disabled>Select One</option>
										@foreach($makam as $makam_value)
										<option value="{{ $makam_value->id }}">{{ $makam_value->makam_name}}</option>
										@endforeach
									</select>
									<span class="text-danger makam-id-error"></span>
								</div>

								<div class="form-group">
									<label for="jenazah_name">Nama Jenazah</label>
									<input type="text" name="jenazah_name" class="form-control" id="edit-jenazah-name" value="{{ old('jenazah_name') }}" placeholder="ex: nama jenazah">
									<span class="text-danger jenazah-name-error"></span>
								</div>

								<div class="form-group">
									<label for="jenazah_nik">NIK</label>
									<input type="number" name="jenazah_nik" class="form-control" id="edit-jenazah-nik" value="{{ old('jenazah_nik') }}" placeholder="ex: 12345678910">
									<span class="text-danger jenazah-nik-error"></span>
								</div>
								<div class="form-group">
									<label for="jenazah_ahli_waris">Ahli Waris</label>
									<input type="text" name="jenazah_ahli_waris" class="form-control" id="edit-jenazah-ahli-waris" value="{{ old('jenazah_ahli_waris') }}" placeholder="ex: ahli waris">
									<span class="text-danger jenazah-ahli-waris-error"></span>
								</div>

								<div class="form-group">
									<label for="jenazah_meninggal_date">Tanggal Meninggal</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" name="jenazah_meninggal_date" class="form-control pull-right" id="edit-jenazah-meninggal-date">
									</div>
									<span class="text-danger jenazah-meninggal-date-error"></span>
								</div>

								<div class="form-group">
									<label for="jenazah_kubur_date">Tanggal Kubur</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" name="jenazah_kubur_date" class="form-control pull-right" id="edit-jenazah-kubur-date">
									</div>
									<span class="text-danger jenazah-kubur-date-error"></span>
								</div>


								
								<div class="form-group">
									<label>Agama</label>
									<select class="form-control" name="jenazah_agama" id="edit-jenazah-agama">
										<option selected disabled>Select One</option>
										<option value="Islam">Islam</option>
										<option value="Katolik">Katolik</option>
										<option value="Protestan">Protestan</option>
										<option value="Hindu">Hindu</option>
										<option value="BUdha">BUdha</option>
										<option value="Konghucu">Konghucu</option>
										<option value="Lainnya">Lainnya</option>
									</select>
									<span class="text-danger jenazah-agama-error"></span>
								</div>

								<div class="form-group">
									<label>Status</label>
									<select class="form-control" name="jenazah_status" id="edit-jenazah-status">
										<option selected disabled>Select One</option>
										<option value="Single">Single</option>
										<option value="Menikah">Menikah</option>
										<option value="Janda">Janda</option>
										<option value="Duda">Duda</option>
									</select>
									<span class="text-danger jenazah-status-error"></span>
								</div>

								<div class="form-group">
									<label>Publication Status</label>
									<select class="form-control" name="publication_status" id="edit-publication-status">
										<option selected disabled>Select One</option>
										<option value="1">Published</option>
										<option value="0">Unpublished</option>
									</select>
									<span class="text-danger publication-status-error"></span>
								</div>


							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-info btn-flat update-button">Update</button>
							</div>
						</form>

					</div>
				</div>
		</div>
				<!-- /.edit jenazah modal -->

		<!-- view user modal -->
		<div id="user-view-modal" class="modal fade bs-example-modal-lg print-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="btn-group pull-right no-print">
							<div class="btn-group">
								<button class="tip btn btn-default btn-flat btn-sm" id="print-button" data-toggle="tooltip" data-original-title="Print">
									<i class="fa fa-print"></i>
									<span class="hidden-sm hidden-xs"></span>
								</button>
							</div>
							<div class="btn-group">
								<button class="tip btn btn-default btn-flat btn-sm" data-toggle="tooltip" data-original-title="Close" data-dismiss="modal" aria-label="Close">
									<i class="fa fa-remove"></i>
									<span class="hidden-sm hidden-xs"></span>
								</button>
							</div>
						</div>
						<h4 class="modal-title" id="view-name"></h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-9">
								<table class="table table-bordered table-striped">
									<tbody>
										<tr>
											<td width="20%">Role</td>
											<td id="view-role"></td>
										</tr>
										<tr>
											<td>Username</td>
											<td id="view-username"></td>
										</tr>
										<tr>
											<td>Email</td>
											<td id="view-email"></td>
										</tr>
										<tr>
											<td>Gender</td>
											<td id="view-gender"></td>
										</tr>
										<tr>
											<td>Phone</td>
											<td id="view-phone"></td>
										</tr>
										<tr>
											<td>Address</td>
											<td id="view-address"></td>
										</tr>
										<tr>
											<td>Facebook</td>
											<td id="view-facebook"></td>
										</tr>
										<tr>
											<td>Twitter</td>
											<td id="view-twitter"></td>
										</tr>
										<tr>
											<td>Google Plus</td>
											<td id="view-google-plus"></td>
										</tr>
										<tr>
											<td>Linkedin</td>
											<td id="view-linkedin"></td>
										</tr>
										<tr>
											<td>Status</td>
											<td id="view-status"></td>
										</tr>
										<tr>
											<td>About</td>
											<td id="view-about"></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-3">
								<img id="view-avatar" class="img-responsive img-thumbnail img-rounded">
							</div>
						</div>
					</div>
					<div class="modal-footer no-print">
						<button type="button" class="btn btn-default btn-flat" data-dismiss="modal" aria-label="Close">Close</button>
					</div>
				</div>
			</div>
		</div>
				<!-- /.view user modal -->
	@endsection

	@section('script')
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/datatables.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.flash.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/pdfmake.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/vfs_fonts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.html5.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.print.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.colVis.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/js/bootstrap-datepicker.min.js') }}"></script>

	<script type="text/javascript">
		/** Load datatable **/
		$(document).ready(function() {
			get_table_data();

			var date = new Date();
			//date.setDate(date.getDate()-1);
	        $('.date').datepicker({
	            autoclose: true,
	            format: "yyyy-mm-dd",
	            startDate: date,
	        });
	        $('.date').datepicker('setDate', 'now');

		});

		/** Edit **/
		$("#jenazah-table").on("click", ".edit-button", function(){
			var jenazah_id = $(this).data("id");
			var url = "{{ route('admin.jenazah.show', 'jenazah_id') }}";
			url = url.replace("jenazah_id", jenazah_id);
			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(data){
					$('#edit-modal').modal('show');

					$('#edit-modal').modal('show');

					$('#edit-makam-id').val(data['id']);
					$('#edit-jenazah-name').val(data['jenazah_name']);
					$('#edit-jenazah-nik').val(data['jenazah_nik']);
					$('#edit-jenazah-ahli-waris').val(data['jenazah_ahli_waris']);
					$('#edit-jenazah-meninggal-date').val(data['jenazah_meninggal_date']);
					$('#edit-jenazah-kubur-date').val(data['jenazah_kubur_date']);
					$('#edit-jenazah-agama').val(data['jenazah_agama']);
					$('#edit-jenazah-status').val(data['jenazah_status']);
					$('#edit-publication-status').val(data['publication_status']);
				}});
		});

		/** Update **/
		$(".update-button").click(function(){
			var jenazah_id = $('#edit-makam-id').val();
			var url = "{{ route('admin.jenazah.update', 'jenazah_id') }}";
			url = url.replace("jenazah_id", jenazah_id);
			var jenazah_edit_form = $("#jenazah_edit_form");
			var form_data = jenazah_edit_form.serialize();
			$( '.makam-id-error' ).html( "" );
			$( '.jenazah-name-error' ).html( "" );
			$( '.jenazah-nik-error' ).html( "" );
			$( '.jenazah-ahli-waris-error' ).html( "" );
			$( '.jenazah-meninggal-date-error' ).html( "" );
			$( '.jenazah-kubur-date-error' ).html( "" );
			$( '.jenazah-agama-error' ).html( "" );
			$( '.jenazah-status-error' ).html( "" );
			$( '.publication-status-error' ).html( "" );
			$.ajax({
				url: url,
				type:'POST',
				data:form_data,
				success:function(data) {
					console.log(data);
					if(data.errors) {

						if(data.errors.makam_id){
							$( '.makam-id-error' ).html( data.errors.makam_id[0] );
						}
						if(data.errors.jenazah_name){
							$( '.jenazah-name-error' ).html( data.errors.jenazah_name[0] );
						}
						if(data.errors.jenazah_nik){
							$( '.jenazah-nik-error' ).html( data.errors.jenazah_nik[0] );
						}
						if(data.errors.jenazah_ahli_waris){
							$( '.jenazah-ahli-waris-error' ).html( data.errors.jenazah_ahli_waris[0] );
						}
						if(data.errors.jenazah_meninggal_date){
							$( '.jenazah-meninggal-date-error' ).html( data.errors.jenazah_meninggal_date[0] );
						}
						if(data.errors.jenazah_kubur_date){
							$( '.jenazah-kubur-date-error' ).html( data.errors.jenazah_kubur_date[0] );
						}
						if(data.errors.jenazah_agama){
							$( '.jenazah-agama-error' ).html( data.errors.jenazah_agama[0] );
						}
						if(data.errors.jenazah_status){
							$( '.jenazah-status-error' ).html( data.errors.jenazah_status[0] );
						}
						if(data.errors.publication_status){
							$( '.publication-status-error' ).html( data.errors.publication_status[0] );
						}
					}
					if(data.success) {
						window.location.href = '{{ route('admin.jenazah.index') }}';
					}
				},
			});
		});

		/** Delete **/
		$("#jenazah-table").on("click", ".delete-button", function(){
			var jenazah_id = $(this).data("id");
			var url = "{{ route('admin.jenazah.destroy', 'jenazah_id') }}";
			url = url.replace("jenazah_id", jenazah_id);
			$('#delete-modal').modal('show');
			$('#delete_form').attr('action', url);
		});

		/** View **/
		$("#jenazah-table").on("click", ".view-button", function(){
			var jenazah_id = $(this).data("id");
			var url = "{{ route('admin.jenazah.show', 'jenazah_id') }}";
			url = url.replace("jenazah_id", jenazah_id);
			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(data){
					$('#view-modal').modal('show');
					$('#view-jenazah-name').text(data['jenazah_name']);

					$('#view-makam-id').text(data.makam.makam_name);
					$('#view-jenazah-nik').text(data['jenazah_nik']);
					$('#view-jenazah-ahli-waris').text(data['jenazah_ahli_waris']);
					$('#view-jenazah-meninggal-date').text(data['jenazah_meninggal_date']);
					$('#view-jenazah-kubur-date').text(data['jenazah_kubur_date']);
					$('#view-jenazah-agama').text(data['jenazah_agama']);
					$('#view-jenazah-status').text(data['jenazah_status']);

					if(data['publication_status'] == 1){
						$('#view-publication-status').text('Published');
					}else{
						$('#view-publication-status').text('Unpublished');
					}
				}});
		});

		/** Add **/
		$(".add-button").click(function(){
			$('#add-modal').modal('show');
		});

		/** Store **/
		$("#store-button").click(function(){
			var jenazah_add_form = $("#jenazah_add_form");
			var form_data = jenazah_add_form.serialize();
			$( '#makam-id-error' ).html( "" );
			$( '#jenazah-name-error' ).html( "" );
			$( '#jenazah-nik-error' ).html( "" );
			$( '#jenazah-ahli-waris-error' ).html( "" );
			$( '#jenazah-meninggal-date-error' ).html( "" );
			$( '#jenazah-kubur-date-error' ).html( "" );
			$( '#jenazah-agama-error' ).html( "" );
			$( '#jenazah-status-error' ).html( "" );
			$( '#publication-status-error' ).html( "" );
			$.ajax({
				url:'{{ route('admin.jenazah.store') }}',
				type:'POST',
				data:form_data,
				success:function(data) {
					console.log(data);
					if(data.errors) {
						if(data.errors.makam_id){
							$( '#makam-id-error' ).html( data.errors.makam_id[0] );
						}
						if(data.errors.jenazah_name){
							$( '#jenazah-name-error' ).html( data.errors.jenazah_name[0] );
						}
						if(data.errors.jenazah_nik){
							$( '#jenazah-nik-error' ).html( data.errors.jenazah_nik[0] );
						}
						if(data.errors.jenazah_ahli_waris){
							$( '#jenazah-ahli-waris-error' ).html( data.errors.jenazah_ahli_waris[0] );
						}
						if(data.errors.jenazah_meninggal_date){
							$( '#jenazah-meninggal-date-error' ).html( data.errors.jenazah_meninggal_date[0] );
						}
						if(data.errors.jenazah_kubur_date){
							$( '#jenazah-kubur-date-error' ).html( data.errors.jenazah_kubur_date[0] );
						}
						if(data.errors.jenazah_agama){
							$( '#jenazah-agama-error' ).html( data.errors.jenazah_agama[0] );
						}
						if(data.errors.jenazah_status){
							$( '#jenazah-status-error' ).html( data.errors.jenazah_status[0] );
						}
						if(data.errors.publication_status){
							$( '#publication-status-error' ).html( data.errors.publication_status[0] );
						}
					}
					if(data.success) {
						window.location.href = '{{ route('admin.jenazah.index') }}';
					}
				},
			});
		});

		/** Get datatable **/
		function get_table_data(){
			$('#jenazah-table').DataTable({
				dom: 'Blfrtip',
				buttons: [
				{ extend: 'copy', exportOptions: { columns: ':visible'}},
				{ extend: 'print', exportOptions: { columns: ':visible'}},
				{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
				{ extend: 'csv', exportOptions: { columns: ':visible'}},
				{ extend: 'colvis', text:'Column'},
				],
				columnDefs: [ {
					targets: -1,
					visible: true
				} ],
				lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				processing: true,
				serverSide: true,
				ajax: "{{ route('admin.getJenazahRoute') }}",
				columns: [
				{data: 'makam_name'},
				{data: 'jenazah_name'},
				{data: 'jenazah_nik'},
				{data: 'jenazah_meninggal_date'},
				{data: 'created_at'},
				{data: 'updated_at'},
				{data: 'publication_status', name: 'publication_status', orderable: false, searchable: false},
				{data: 'action', name: 'action', orderable: false, searchable: false},
				],
				order: [[3, 'desc']],
			});
		}

		/** User View **/
		$("#jenazah-table").on("click", ".user-view-button", function(){
			var id = $(this).data("id");
			var url = "{{ route('admin.users.show', 'id') }}";
			url = url.replace("id", id);
			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(data){
					var src = '{{ asset('public/avatar') }}/';
					var default_avatar = '{{ asset('public/avatar/user.png') }}';
					$('#user-view-modal').modal('show');

					$('#view-name').text(data['name']);
					$('#view-username').text(data['username']);
					$('#view-email').text(data['email']);
					$("#view-avatar").attr("src", src+data['avatar']);
					if(data['avatar']){
						$("#view-avatar").attr("src", src+data['avatar']);
					}else{
						$("#view-avatar").attr("src", default_avatar);
					}
					if(data['gender'] == 'm'){
						$('#view-gender').text('Male');
					}else{
						$('#view-gender').text('Female');
					}
					$('#view-phone').text(data['phone']);
					$('#view-address').text(data['address']);
					$('#view-facebook').text(data['facebook']);
					$('#view-twitter').text(data['twitter']);
					$('#view-google-plus').text(data['google_plus']);
					$('#view-linkedin').text(data['linkedin']);
					$('#view-about').text(data['about']);
					if(data['role'] == 'admin'){
						$('#view-role').text('Admin');
					}else{
						$('#view-role').text('User');
					}
					if(data['activation_status'] == 1){
						$('#view-status').text('Active');
					}else{
						$('#view-status').text('Block');
					}
				}});
		});
	</script>
	@endsection