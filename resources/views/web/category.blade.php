@extends('web.layouts.app')

@section('title', $category->meta_title)
@section('keywords', $category->meta_keywords)
@section('description', $category->meta_description)

@section('style')
@endsection

@section('content')


<!-- Header-->
<header data-background="{{ asset('public/web/img/header/10.jpg') }}" class="intro introhalf">
  <!-- Intro Header-->
  <div class="intro-body">
    <h1>{{ $category->category_name }}</h1>
    <h5><a href="{{ route('homePage') }}">Home</a> / {{ $category->category_name }}</h5>
  </div>
</header>


    <!-- News Block-->
    <section id="news" class="section-small">
      <div class="container">
        <div class="row grid-pad">
          <div class="col-md-8">
            <h3 class="pull-left">{{ $category->category_name }}</h3>
            <div class="pull-right">
              <h4>OUR LATEST NEWS</h4>
            </div>
            <div class="clearfix"></div>
            <div class="row">

						@foreach($posts as $post)
						<div class="col-sm-6">
							<a href="{{ route('detailsPage', $post->post_slug) }}">
								<img src="{{ get_featured_image_thumbnail_url($post->featured_image) }}" alt="" class="img-responsive center-block">
							  <h5>{{ str_limit($post->post_title, 44) }}</h5>
							</a>
							{!! str_limit($post->post_details, 120) !!}
							<a href="{{ route('detailsPage', $post->post_slug) }}" class="btn btn-gray btn-xs">Read more</a>
						</div>
						@endforeach

            </div>
          </div>

					@include('web.includes.sidebar')

        </div>
      </div>
    </section>
    <!-- Pagination-->
    <div class="section section-small bg-white">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <nav>

        		{{ $posts->links() }}
              
            </nav>
          </div>
        </div>
      </div>
    </div>


@endsection

@section('sidebar')
@endsection

@section('script')
@endsection