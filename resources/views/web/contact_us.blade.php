@extends('web.layouts.app')
@section('title', 'Contact Us')

@section('style')
@endsection

@section('content')

<!-- Header-->
<header data-background="{{ asset('public/web/img/header/10.jpg') }}" class="intro introhalf">
  <!-- Intro Header-->
  <div class="intro-body">
    <h1>Kontak Kami</h1>
    <h5><a href="{{ route('homePage') }}">Home</a> / Kontak Kami</h5>
  </div>
</header>


<!-- Contact Section-->
<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h2>contact us</h2>
        <p>Feel free to contact us to provide some feedback on our templates, give us suggestions for new templates and themes, or to just say hello! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar.</p>
        <hr>
        <h5><i class="fa fa-map-marker fa-fw fa-lg"></i> {{ $setting->address_line_one }}
        </h5>
        <h5><i class="fa fa-envelope fa-fw fa-lg"></i> {{ $setting->email }}
        </h5>
        <h5><i class="fa fa-phone fa-fw fa-lg"></i> {{ $setting->phone }}
        </h5>
      </div>
      <div class="col-md-5 col-md-offset-2">
        <h2>Say hello</h2>
        <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. For more information on how to do this please visit the Docs!-->
        <form id="contactForm" name="sentMessage" novalidate="">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="name" class="sr-only control-label">You Name</label>
              <input id="name" type="text" placeholder="You Name" required="" data-validation-required-message="Please enter name" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="email" class="sr-only control-label">You Email</label>
              <input id="email" type="email" placeholder="You Email" required="" data-validation-required-message="Please enter email" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="phone" class="sr-only control-label">You Phone</label>
              <input id="phone" type="tel" placeholder="You Phone" required="" data-validation-required-message="Please enter phone number" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="message" class="sr-only control-label">Message</label>
              <textarea id="message" rows="2" placeholder="Message" required="" data-validation-required-message="Please enter a message." aria-invalid="false" class="form-control input-lg"></textarea><span class="help-block text-danger"></span>
            </div>
          </div>
          <div id="success"></div>
          <button type="submit" class="btn btn-dark">Send</button>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- Map Section-->
<div id="map">
<!-- <iframe src="https://www.google.com/maps/embed?pb=" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.717398873295!2d107.44500011477088!3d-6.557314795257374!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e690e4ef9d22ce7%3A0x6b12d62f529446a7!2sDinas+Tata+Ruang+dan+Permukiman+Kabupaten+Purwakarta!5e0!3m2!1sid!2sid!4v1542672472862" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
@endsection

@section('script')
<!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/-->
    <!-- <script src="https://maps.googleapis.com/maps/api/js"></script> -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3E86i8mx1BZDlAaLcknh_mWl4F70i4os"></script> -->
<!-- <script src="{{ asset('public/web/js/map.js') }}"></script> -->
@endsection