@php
use Carbon\Carbon;
@endphp

@extends('web.layouts.app')

@section('title', $post->meta_title)
@section('keywords', $post->meta_keywords)
@section('description', $post->meta_description)

@section('style')
@endsection

@section('content')

<!-- Header-->
<header data-background="{{ asset('public/web/img/header/10.jpg') }}" class="intro introhalf">
  <!-- Intro Header-->
  <div class="intro-body">
    <h1>{{ $post->post_title }}</h1>
    <h5><a href="{{ route('homePage') }}">Home</a> / <a href="{{ route('categoryPage', $post->category->id) }}" title="{{ $post->category->category_name }}">{{ $post->category->category_name }}</a> / {{ $post->post_title }}</h5>
  </div>
</header>


<section id="news-single" class="section-small">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h4>{{ $post->post_title }}</h4>

		<img src="{{ get_featured_image_url($post->featured_image) }}" alt="{{ $post->post_title }}" class="img-responsive center-block">

		{!! $post->post_details !!}
		@if(!empty($post->youtube_video_url))
		<iframe width="100%" height="420" src="{{ $post->youtube_video_url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		@endif

      </div>

		@include('web.includes.sidebar')


    </div>
  </div>
</section>
<section class="section-small bg-white">
  <div class="container grid-pad">
    <h3>{{ $post->category->category_name }} Terbaru</h3>
    <div class="row">

		@foreach($related_posts->chunk(3) as $items)
		<div class="progress-unit">
			@foreach($items as $related_post)


		      <div class="col-sm-6 col-md-4"><a href="{{ route('detailsPage', $related_post->post_slug) }}"><img src="{{ get_featured_image_thumbnail_url($related_post->featured_image) }}" alt="" class="img-responsive center-block"/>
	          	<h5>{{ str_limit($related_post->post_title, 50) }}</h5></a>
		        {!! str_limit($related_post->post_details, 50) !!}
            	<a href="{{ route('detailsPage', $related_post->post_slug) }}" class="btn btn-gray btn-xs">Read more</a>

		      </div>
			@endforeach
		</div>
		@endforeach
    </div>
  </div>
</section>

@endsection

@section('sidebar')
@endsection

@section('script')
@endsection