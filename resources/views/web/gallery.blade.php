@extends('web.layouts.app')

@section('title', 'Gallery')
@section('keywords', $setting->gallery_meta_keywords)
@section('description', $setting->gallery_meta_description)

@section('style')
@endsection

@section('content')


<!-- Header-->
<header data-background="{{ asset('public/web/img/header/10.jpg') }}" class="intro introhalf">
  <!-- Intro Header-->
  <div class="intro-body">
    <h1>Gallery</h1>
    <h5><a href="{{ route('homePage') }}">Home</a> / Gallery</h5>
  </div>
</header>
<!-- Portfolio-->
<section id="portfolio" class="portfolio-wide no-pad-btm">
  <div class="container">
    <div id="grid" class="row portfolio-items">


	@foreach($galleries as $gallery)

			
	<div data-groups="[&quot;design&quot;, &quot;branding&quot;]" class="col-sm-3">
		<div class="portfolio-item">
			<a class="post-music view-image" data-id="{{ $gallery->id }}" title="{{ $gallery->caption }}">
			<!-- <a href="{{ $gallery->id }}"> -->
				<img src="{{ get_gallery_image_url($gallery->image) }}" alt="{{ $gallery->caption }}">
			    <div class="portfolio-overlay">
			      <div class="caption">
			        <h5>{{ $gallery->caption }}</h5><!-- <span>Lorem ipsum dolor sit amet</span> -->
			      </div>
			    </div>
			</a>
		</div>
	</div>
	@endforeach
      <!-- sizerli.span3.m-span3.shuffle_sizer
      -->
    </div>
  </div>
</section>
<!-- Pagination-->
<div class="section section-small bg-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <nav>
			{{ $galleries->links() }}
          <!-- <ul class="pagination">
            <li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
            <li class="active"><a href="#">1<span class="sr-only">(current)</span></a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">&middot;&middot;&middot;</a><a href="#">38</a></li>
            <li><a href="#" aria-label="Previous"><span aria-hidden="true">&raquo;</span></a></li>
          </ul> -->
        </nav>
      </div>
    </div>
  </div>
</div>




<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="view-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img id="view-image" width="100%">
				<br>
				<span class="caption" id="view-caption"></span>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')
@endsection