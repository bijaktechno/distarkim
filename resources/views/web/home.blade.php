@extends('web.layouts.app')

@section('title', $setting->meta_title)
@section('keywords', $setting->meta_keywords)
@section('description', $setting->meta_description)

@section('style')
@endsection

@section('content')
<!-- Header-->
<header class="intro">
  <!-- Intro Header-->
  <div class="intro-body">
    <h1 class="classic">Purwakarta</h1>
    <h4>Dinas <span class="bold">Tata Ruang dan Pemukiman</span> Purwakarta
    </h4>
    <ul class="list-inline lead">
      <li><a href="#services" class="btn btn-border btn-lg page-scroll"><i class="fa fa-chevron-down"></i> WHO WE ARE</a></li>
      <li><a href="#contact" class="btn btn-white btn-lg page-scroll"><i class="fa fa-chevron-down"></i> CONTACT US</a></li>
    </ul>
  </div>
</header>
<!-- About Section-->
<!-- <section id="about">
  <div class="container wow fadeIn">
    <div class="row">
      <div class="col-lg-12">
        <h3>Tentang Kami</h3>
        <p class="no-pad">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.s.</p>
        <h2 class="classic">Distarkim</h2>
      </div>
    </div>
  </div>
</section> -->
<!-- Slider 2 Section-->
<!-- <section id="about-slider2" class="section-small bg-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <h3>Modern Portfolio</h3>
        <p>Amazing solution for portfolio websites which you can fit for freelancers, photographers or agency showcase. Pheromone easily and efficiently scales your project with one code base.</p><a href="portfolio-masonry-4.html" class="btn btn-dark">Check it out</a>
      </div>
      <div data-wow-duration="2s" data-wow-delay=".2s" class="col-lg-5 col-lg-offset-1 wow zoomIn">
        <div id="carousel-light2" class="carousel slide carousel-fade">
          <ol class="carousel-indicators">
            <li data-target="#carousel-light2" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-light2" data-slide-to="1"></li>
            <li data-target="#carousel-light2" data-slide-to="2"></li>
          </ol>
          <div role="listbox" class="carousel-inner">
            <div class="item active"><img src="{{ asset('public/web/img/slider/6.png') }}" alt="" class="img-responsive center-block"></div>
            <div class="item"><img src="{{ asset('public/web/img/slider/8.png') }}" alt="" class="img-responsive center-block"></div>
            <div class="item"><img src="{{ asset('public/web/img/slider/7.png') }}" alt="" class="img-responsive center-block"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> -->
<!-- Services Section-->
<section id="services">
  <div class="container text-center">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <h3>Tugas dan Fungsi</h3>
        <p>Tugas Pokok Dinas Tata Ruang dan Pemukiman adalah melaksanakan sebagian urusan pemerintahan daerah di bidang pekerjaan umum lingkup keciptakaryaan, penataan ruang, perumahan dan permukiman berdasarkan asas otonomi dan tugas pembantuan.
        </p>

      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-sm-6">
        <h4><i class="icon icon-big ion-ios-analytics-outline"></i></h4>
        <p>Perumusan kebijakan teknis bidang keciptakaryaan, penataan ruang, dan perumahan</p>
      </div>
      <div class="col-lg-4 col-sm-6">
        <h4><i class="icon icon-big ion-ios-pie-outline"></i></h4>
        <p>Penyelenggaraan sebagian urusan pemerintahan dan pelayanan umum di bidang pekerjaan umum lingkup keciptakaryaan, penataan ruang dan perumahan</p>
      </div>
      <div class="col-lg-4 col-sm-6">
        <h4><i class="icon icon-big ion-ios-glasses-outline"></i></h4>
        <p>Pembinaan dan pelaksanaan tugas yang meliputi bidang tata bangunan, pembinaan, pengawasan dan pengendalian, perumahan, permukiman, dan tata ruang</p>
      </div>
    </div>
  </div>
</section>
<!-- Services 2 Section-->
<section class="section-small bg-img text-center">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div data-wow-delay=".2s" class="col-lg-6 col-sm-6 wow fadeIn">
        <h4><i class="icon icon-big icon ion-ios-infinite-outline"></i></h4>
        <p>Pelaksanaan pelayanan teknis administratif dinas</p>
      </div>
      <div data-wow-delay=".4s" class="col-lg-6 col-sm-6 wow fadeIn">
        <h4><i class="icon icon-big ion-ios-nutrition-outline"></i></h4>
        <p>Pelaksanaan tugas lain yang diberikan oleh bupati sesuai dengan tugas dan fungsinya</p>
      </div>
    </div>
  </div>
</section>
<!-- News Block-->
<section id="news">
  <div class="container">
    <h3 class="pull-left">Berita</h3>
    <div class="pull-right">
      <h5><a href="{{ route('categoryPage', 1) }}">LIHAT SEMUA BERITA</a></h5>
    </div>
    <div class="clearfix"></div>
    <div class="row grid-pad">


        @foreach($popular_posts as $related_post)
          <div class="col-sm-4 col-md-4"><a href="{{ route('detailsPage', $related_post->post_slug) }}"><img src="{{ get_featured_image_thumbnail_url($related_post->featured_image) }}" alt="" class="img-responsive center-block"/>
              <h5>{{ str_limit($related_post->post_title, 50) }}</h5></a>
            {!! str_limit($related_post->post_details, 50) !!}
            <a href="{{ route('detailsPage', $related_post->post_slug) }}" class="btn btn-gray btn-xs">Read more</a>
          </div>
        @endforeach


    </div>
  </div>
</section>
<!-- Quotes-->
<section class="section-small bg-img3 text-center">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <p><i class="icon fa fa-quote-left fa-lg"></i></p>
        <h4>A business has to be involving, it has to be fun, and it has to exercise your creative instincts. Start where you are. Use what you have. Do what you can.</h4>
        <h2 class="no-pad classic">James Daniels</h2>
      </div>
    </div>
  </div>
</section>

<!-- Contact Section-->
<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h2>contact us</h2>
        <p>Feel free to contact us to provide some feedback on our templates, give us suggestions for new templates and themes, or to just say hello! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar.</p>
        <hr>
        <h5><i class="fa fa-map-marker fa-fw fa-lg"></i> {{ $setting->address_line_one }}
        </h5>
        <h5><i class="fa fa-envelope fa-fw fa-lg"></i> {{ $setting->email }}
        </h5>
        <h5><i class="fa fa-phone fa-fw fa-lg"></i> {{ $setting->phone }}
        </h5>
      </div>
      <div class="col-md-5 col-md-offset-2">
        <h2>Say hello</h2>
        <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. For more information on how to do this please visit the Docs!-->
        <form id="contactForm" name="sentMessage" novalidate="">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="name" class="sr-only control-label">You Name</label>
              <input id="name" type="text" placeholder="You Name" required="" data-validation-required-message="Please enter name" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="email" class="sr-only control-label">You Email</label>
              <input id="email" type="email" placeholder="You Email" required="" data-validation-required-message="Please enter email" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="phone" class="sr-only control-label">You Phone</label>
              <input id="phone" type="tel" placeholder="You Phone" required="" data-validation-required-message="Please enter phone number" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="message" class="sr-only control-label">Message</label>
              <textarea id="message" rows="2" placeholder="Message" required="" data-validation-required-message="Please enter a message." aria-invalid="false" class="form-control input-lg"></textarea><span class="help-block text-danger"></span>
            </div>
          </div>
          <div id="success"></div>
          <button type="submit" class="btn btn-dark">Send</button>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- Map Section-->
<div id="map">
<!-- <iframe src="https://www.google.com/maps/embed?pb=" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.717398873295!2d107.44500011477088!3d-6.557314795257374!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e690e4ef9d22ce7%3A0x6b12d62f529446a7!2sDinas+Tata+Ruang+dan+Permukiman+Kabupaten+Purwakarta!5e0!3m2!1sid!2sid!4v1542672472862" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
@endsection

@section('sidebar')
@endsection

@section('script')
<!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/-->
    <!-- <script src="https://maps.googleapis.com/maps/api/js"></script> -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3E86i8mx1BZDlAaLcknh_mWl4F70i4os"></script> -->
<!-- <script src="{{ asset('public/web/js/map.js') }}"></script> -->
<!-- Background Slider-->
<script src="{{ asset('public/web/js/vegas/vegas.min.js') }}"></script>
<script>
  $('body').vegas({
      delay: 9000,
      timer: false,
      transitionDuration: 2000,
      slides: [
          {src: "{{ asset('public/web/img/header/0.jpg') }}"},
          {src: "{{ asset('public/web/img/header/1.jpg') }}"},
          {src: "{{ asset('public/web/img/header/2.jpg') }}"}
      ],
      transition: 'fade',
      animation: 'kenburns'
  });
</script>
@endsection