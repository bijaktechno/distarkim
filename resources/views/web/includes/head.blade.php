<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="{{ asset('public/web/favicon/favicon.png') }}">
<meta name="description" content="">
<meta name="author" content="">
<title>@yield('title') - {{ $setting->website_title }}</title>
<!-- Bootstrap Core CSS-->
<link href="{{ asset('public/web/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Custom CSS-->
<link href="{{ asset('public/web/css/pheromone.css') }}" rel="stylesheet">