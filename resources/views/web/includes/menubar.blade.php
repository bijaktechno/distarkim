<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
  <ul class="nav navbar-nav">
    <!-- Hidden li included to remove active class from about link when scrolled up past about section-->
    <li class="hidden"><a href="#page-top"></a></li>
    <li><a href="{{ route('homePage') }}">Beranda</a></li>
    <li><a href="#">Profil <i class="fa fa-angle-down"></i><span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('pagePage', 'struktur_organisasi') }}">Struktur Organisasi</a></li>
            <li><a href="{{ route('pagePage', 'tugas_dan_fungsi') }}">Tugas & Fungsi</a></li>
            <li><a href="{{ route('pagePage', 'visi_misi') }}">Visi & Misi</a></li>
            <li><a href="{{ route('pagePage', 'tupoksi') }}">Tupoksi</a></li>
            <!-- <li><strong class="text-muted">Multi Page</strong></li> -->
        </ul>
    </li>
    <li><a href="{{ route('categoryPage', 1) }}">Berita</a></li>
    <li><a href="#">Informasi <i class="fa fa-angle-down"></i><span class="caret"></span></a>
        <ul class="dropdown-menu">
        <!-- <ul class="dropdown-menu columns-3"> -->
            <li><a href="{{ route('pagePage', 'data_distarkim') }}">Data Distarkim</a></li>
            <li><a href="{{ route('pagePage', 'imb') }}">IMB</a></li>
            <li><a href="{{ route('pagePage', 'peraturan_perundang_undangan') }}">Perundang-Undangan</a></li>
            <li><a href="{{ route('categoryPage', 2) }}">Pengumuman</a></li>
            <li><a href="{{ route('makamPage') }}">Makam</a></li>
            <li><a href="{{ route('jenazahPage') }}">Pelayanan Registrasi TPU</a></li>
            <li><a href="{{ route('pagePage', 'layanan') }}">Layanan</a></li>
        </ul>
    </li>
    <li><a href="{{ route('galleryPage') }}">Galeri</a></li>
    <li><a href="{{ route('contactUsPage') }}">Kontak</a></li>
  </ul>
</div>