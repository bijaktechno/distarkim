
          <div class="col-md-3 col-md-offset-1">
            <!-- <h4>search</h4>


                <form data-parsley-validate action="{{ route('searchRoute') }}" method="post" class="form-inline subscribe-form">
                    {{ csrf_field() }}
              <div class="input-group input-group-lg">
                    <input type="text" name="search_keywords" class="form-control" placeholder="ex: post title" required>
                    <span class="input-group-btn">
                        <button class="btn btn-dark" type="submit"><i class="fa fa-search fa-lg"></i></button>
                      </span>

              </div>
                </form>
                @if ($errors->has('search_keywords'))
                <p class="text-center text-danger" style="font-size: 12px; padding: 5px 0px;">{{ $errors->first('search_keywords') }}</p>
                @endif

            <hr> -->
            <h4>Facebook</h4>

                        @if(!empty($setting->facebook))
                                <div class="fb-page" data-href="{{ $setting->facebook }}" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                    <blockquote cite="{{ $setting->facebook }}" class="fb-xfbml-parse-ignore">
                                        <a href="{{ $setting->facebook }}">{{ $setting->website_title }}</a>
                                    </blockquote>
                                </div>
                                <div id="fb-root"></div>
                                <script type="text/javascript">
                                    (function(d, s, id) {
                                      var js, fjs = d.getElementsByTagName(s)[0];
                                      if (d.getElementById(id)) return;
                                      js = d.createElement(s); js.id = id;
                                      js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1227264524030761&autoLogAppEvents=1';
                                      fjs.parentNode.insertBefore(js, fjs);
                                  }(document, 'script', 'facebook-jssdk'));
                              </script>
                          @endif
            <hr>
            <h4>subscribe</h4>
            <p>Sign up with your email address to receive news and updates.</p>
            <!-- MailChimp Signup Form - Replace the form action in the line below with your MailChimp embed action! For more information on how to do this please visit the Docs!-->

                    <form data-parsley-validate id="subscribe_add_form" method="post" class="form-inline subscribe-form">
                        {{ csrf_field() }}
              <div class="input-group input-group-lg">

                        <input type="text" name="email" class="newsletter-email form-control" placeholder="ex: mail@mail.com" required>
                        <span class="input-group-btn">
                            <button class="newsletter-subscribe btn btn-dark" name="subscribe" type="button" id="store-button">go</button>
                          </span>
              </div>
                    </form>
                    <p class="text-center text-danger" id="email-error"></p>
                    <p class="text-center text-success" id="email-success"></p>

            <!-- End MailChimp Signup Form--><img src="img/misc/mailchimp.png" alt="">
          </div>