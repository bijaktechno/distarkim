@extends('web.layouts.app')
@section('title', 'Pelayanan Registrasi TPU')


@section('style')
	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/dataTables.bootstrap.min.css')}}" />

	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/buttons.dataTables.min.css')}}">
@endsection

@section('content')

<!-- Header-->
<header data-background="{{ asset('public/web/img/header/10.jpg') }}" class="intro introhalf">
  <!-- Intro Header-->
  <div class="intro-body">
    <h1>Pelayanan Registrasi TPU</h1>
    <h5><a href="{{ route('homePage') }}">Home</a> / Pelayanan Registrasi TPU</h5>
  </div>
</header>


<section id="" class="section-small">
  <div class="container">
		<div class="table-responsive">
			<table id="posts-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0">
				<thead>
					<tr>
						<th>Nama Kelurahan</th>
						<th>Nama Makam</th>
						<th>Nama Jenazah</th>
						<th>NIK</th>
						<th>Ahli Waris</th>
						<th>Tanggal Meninggal</th>
						<th>Tanggal dikubur</th>
						<th>Agama</th>
						<th>Status</th>
						<th>Created At</th>
						<th>Updated At</th>
					</tr>
				</thead>
			</table>
		</div>

  </div>
</section>

@endsection

@section('sidebar')
@endsection

@section('script')
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/datatables.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.flash.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/pdfmake.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/vfs_fonts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.html5.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.print.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.colVis.min.js') }}"></script>

	<script type="text/javascript">
		/** Load datatable **/
		$(document).ready(function() {
			get_table_data();
		});
		function get_table_data(){
			$('#posts-table').DataTable({
				dom: 'Blfrtip',
				buttons: [
				{ extend: 'copy', exportOptions: { columns: ':visible'}},
				{ extend: 'print', exportOptions: { columns: ':visible'}},
				{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
				{ extend: 'csv', exportOptions: { columns: ':visible'}},
				{ extend: 'colvis', text:'Column'},
				],
				columnDefs: [ {
					targets: -1,
					visible: true
				} ],
				lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				processing: true,
				serverSide: true,
				ajax: "{{ route('getJenazah') }}",
				columns: [
				{data: 'kelurahan_name'},
				{data: 'makam_name'},
				{data: 'jenazah_name', orderable: true, searchable: true},
				{data: 'jenazah_nik', orderable: true, searchable: true},
				{data: 'jenazah_ahli_waris', orderable: true, searchable: true},
				{data: 'jenazah_meninggal_date', orderable: true, searchable: true},
				{data: 'jenazah_kubur_date', orderable: true, searchable: true},
				{data: 'jenazah_agama', orderable: true, searchable: true},
				{data: 'jenazah_status', orderable: true, searchable: true},
				{data: 'created_at'},
				{data: 'updated_at'},
				],
				order: [[4, 'desc']],
			});
		}

	</script>
@endsection