<!DOCTYPE html>
<html lang="en">
<!-- head -->
<head>
    @include('web.includes.head')
    @yield('style')
</head>
<!-- /.head -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" class="top">
    <!-- theme layout -->

    <div id="preloader">
      <div id="status"></div>
    </div>

    <!-- header -->
    @include('web.includes.header')
    <!-- /.header -->

    <!-- left section -->
    @yield('content')
    <!-- /. left section -->

    <!-- /. right side widget -->
    @yield('sidebar')
    <!--/. right side widget-->
    <!-- /.first section block -->

    <!-- footer -->
    @include('web.includes.footer')
    <!-- /. footer -->
    <!-- /. theme-layout -->

    <!-- scripts -->
    @include('web.includes.scripts')
    @yield('script')
    <!-- /. script -->
</body>
</html>