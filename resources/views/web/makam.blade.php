@extends('web.layouts.app')
@section('title', 'Makam')


@section('style')
	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/dataTables.bootstrap.min.css')}}" />

	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/buttons.dataTables.min.css')}}">
@endsection

@section('content')

<!-- Header-->
<header data-background="{{ asset('public/web/img/header/10.jpg') }}" class="intro introhalf">
  <!-- Intro Header-->
  <div class="intro-body">
    <h1>Data TPU</h1>
    <h5><a href="{{ route('homePage') }}">Home</a> / Data TPU</h5>
  </div>
</header>


<section id="" class="section-small">
  <div class="container">
		<div class="table-responsive">
			<table id="posts-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0">
				<thead>
					<tr>
						<th>Nama Kelurahan</th>
						<th>Nama Makam</th>
						<th>Alamat Makam</th>
						<th>Created At</th>
						<th>Updated At</th>
					</tr>
				</thead>
			</table>
		</div>

  </div>
</section>

@endsection

@section('sidebar')
@endsection

@section('script')
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/datatables.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.flash.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/pdfmake.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/vfs_fonts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.html5.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.print.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.colVis.min.js') }}"></script>

	<script type="text/javascript">
		/** Load datatable **/
		$(document).ready(function() {
			get_table_data();
		});
		function get_table_data(){
			$('#posts-table').DataTable({
				dom: 'Blfrtip',
				buttons: [
				{ extend: 'copy', exportOptions: { columns: ':visible'}},
				{ extend: 'print', exportOptions: { columns: ':visible'}},
				{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
				{ extend: 'csv', exportOptions: { columns: ':visible'}},
				{ extend: 'colvis', text:'Column'},
				],
				columnDefs: [ {
					targets: -1,
					visible: true
				} ],
				lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				processing: true,
				serverSide: true,
				ajax: "{{ route('getMakam') }}",
				columns: [
				{data: 'kelurahan_name'},
				{data: 'makam_name'},
				{data: 'makam_address', name: 'makam_address', orderable: true, searchable: true},
				{data: 'created_at'},
				{data: 'updated_at'},
				],
				order: [[4, 'desc']],
			});
		}

	</script>
@endsection