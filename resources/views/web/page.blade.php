@extends('web.layouts.app')

@section('title', $page->meta_title)
@section('keywords', $page->meta_keywords)
@section('description', $page->meta_description)

@section('style')
@endsection

@section('content')

<!-- Header-->
<header data-background="{{ asset('public/web/img/header/10.jpg') }}" class="intro introhalf">
  <!-- Intro Header-->
  <div class="intro-body">
    <h1>{{ $page->page_name }}</h1>
    <h5><a href="{{ route('homePage') }}">Home</a> / {{ $page->page_name }}</h5>
  </div>
</header>


<section id="news-single" class="section-small">
  <div class="container">
  	@if($page->page_type == 0)
  		@if($page->page_featured_image)
        <div class="row">
          <div class="col-md-12">
            <p>
              <!-- <a href="portfolio-single.html"> -->
				    <img src="{{ get_page_featured_image_url($page->page_featured_image) }}" alt="{{ $page->page_name }}" class="img-responsive center-block">
            <!-- </a> -->
            </p>
            <!-- p.small Collaboratively administrate empowered markets  via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain-->
          </div>
        </div>
        @endif
        <div class="row">
          <div class="col-md-12">
				<p>{!! $page->page_content !!}</p>

          </div>
        </div>
    @else
        <div class="row wow fadeIn">
          <div class="col-sm-12">
            <div id="accordion" role="tablist" aria-multiselectable="true" class="panel-group">

            	@foreach($page->pagefile as $key => $pagefile)

					<div class="panel panel-default">
						<div id="{{$pagefile->file_title}}" role="tab" class="panel-heading">
						<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$key}}" aria-expanded="{{$key == 0?'true':'false'}}" aria-controls="{{$key}}" {{$key == 0?'class="collapsed"':''}}>{{$pagefile->file_title}}</a></h4>
						</div>
						<div id="{{$key}}" role="tabpanel" aria-labelledby="{{$pagefile->file_title}}" class="panel-collapse collapse {{$key == 0?'in':''}}">
						<div class="panel-body">


							<object data="{{ get_pagefile_url($pagefile->file_path) }}" type="application/pdf" width="100%" height="500px">
						      <p>Alternative text - include a link <a href="{{ get_pagefile_url($pagefile->file_path) }}">to the PDF!</a></p>
						    </object>
						    
						</div>
						</div>
					</div>
            	@endforeach

            </div>
          </div>
      	</div>
    @endif
  </div>
</section>

@endsection

@section('sidebar')
@endsection

@section('script')
@endsection